$(function() {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  // Init Select2
  $('.select2').select2()

  // Init Locales Moment Js
  moment.locale('id');

  // Event when show password is clicked
  $('.show-input-password').on('change', function() {
    if ($('.input-password').attr('type') == 'password') {
      $('.input-password').prop('type', 'text');
      $('.input-password-confirm').prop('type', 'text');
    } else {
      $('.input-password').prop('type', 'password');
      $('.input-password-confirm').prop('type', 'password');
    }
  });
});

// Method Load Content Ajax
function onLoadContent(url, data = {}) {
  let valUrl = url;
  let valData = data;
  let elContent = $('.table-content');
  
  // Check Clear Content
  if (data.hasOwnProperty('contentBody')
  && data.contentBody == true) {
    elContent.find('#body').html('');
  }

  // Show Loading
  elContent.find('#loader').show();

  $.ajax({
    url: valUrl,
    data: valData,
    method: 'POST'
  })
  .done(function(res) {
    let loadHtml = showData(res);

    elContent.find('#body').append(loadHtml);
    
    // Hide Loading
    elContent.find('#loader').hide();
  })
  .fail(function(err) {
    // Hide Loading
    elContent.find('#loader').hide();
  });
}