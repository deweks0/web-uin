<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MParentActivity extends Model
{
    use HasFactory;
    use SoftDeletes;

    public function activities()
    {
        return $this->hasMany(TypeActivity::class);
    }
}
