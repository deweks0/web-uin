<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{
    use HasFactory;
    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function role()
    {
        return $this->belongsTo(MRoleActivity::class, 'role_activity_id', 'id');
    }

    public function semester()
    {
        return $this->belongsTo(MSemester::class);
    }

    public function profileActivities()
    {
        return $this->hasMany(ProfileActivity::class);
    }
}