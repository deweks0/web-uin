<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MActivity extends Model
{
    use HasFactory;
    use SoftDeletes;

    public function parentActivity() {
        return $this->belongsTo(MParentActivity::class, 'code', 'code');
    }
}
