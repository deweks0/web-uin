<?php

use Illuminate\Support\Carbon;
use App\Models\Notification;

if (! function_exists('getMonth')) {
  function getMonth($month = null)
  {
    $arrMonth = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
    
    if ($month != null)
      return $arrMonth[(int) $month - 1];

    return $arrMonth;
  }
}

if (! function_exists('sendNotification')) {
  function sendNotification($valData)
  {
    $data = new Notification;
    $data->user_id = isset($valData['user_id']) ? $valData['user_id'] : null;
    $data->role_id = isset($valData['role_id']) ? $valData['role_id'] : null;
    $data->title = isset($valData['title']) ? $valData['title'] : null;
    $data->message = isset($valData['message']) ? $valData['message'] : null;
    $data->url_direct = isset($valData['url_direct']) ? $valData['url_direct'] : null;
    $data->status_view = 0;
    $data->save();
  }
}

if (! function_exists('sendNotification')) {
  function sendNotification($valData)
  {
    $data = new Notification;
    $data->user_id = isset($valData['user_id']) ? $valData['user_id'] : null;
    $data->role_id = isset($valData['role_id']) ? $valData['role_id'] : null;
    $data->title = isset($valData['title']) ? $valData['title'] : null;
    $data->message = isset($valData['message']) ? $valData['message'] : null;
    $data->url_direct = isset($valData['url_direct']) ? $valData['url_direct'] : null;
    $data->status_view = 0;
    $data->save();
  }
}

if (! function_exists('sendNotification')) {
  function sendNotification($valData)
  {
    $data = new Notification;
    $data->user_id = isset($valData['user_id']) ? $valData['user_id'] : null;
    $data->role_id = isset($valData['role_id']) ? $valData['role_id'] : null;
    $data->title = isset($valData['title']) ? $valData['title'] : null;
    $data->message = isset($valData['message']) ? $valData['message'] : null;
    $data->url_direct = isset($valData['url_direct']) ? $valData['url_direct'] : null;
    $data->status_view = 0;
    $data->save();
  }
}

if (! function_exists('formatDate')) {
  function formatDate($date, $format = 'l, d F Y')
  {
    return Carbon::parse(date($date))->translatedFormat($format);
  }
}

if (! function_exists('checkFormatImg')) {
  function checkFormatImg($fileName)
  {
    $fileExt = explode('.', $fileName);
    $fileExt = end($fileExt);

    $imgFormat = ['jpg', 'jpeg', 'gif', 'png'];
    if (! in_array($fileExt, $imgFormat))
      return false;

    return true;
  }
}

if (! function_exists('showPhotoProfile')) {
  function showPhotoProfile($fileName)
  {
    $result = asset('img/no-profile.svg');
    if ($fileName != null) {
      if (file_exists( public_path().'/uploads/photo_profile/' . $fileName))
      $result = asset('uploads/photo_profile/' . $fileName);
    }

    return $result;
  }
}