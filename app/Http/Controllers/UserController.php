<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;
use DataTables;
use Auth;
use DB;

use App\Models\MRoleUser;
use App\Models\MSemester;
use App\Models\User;
use App\Models\Profile;

class UserController extends Controller
{
    private $className;

    /**
     * Constructor.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->className = $this->getClassName(get_class($this));
    }

    /**
     * Display page login
     *
     * @return -
     */
    public function indexLogin()
    {
        return $this->getViewPage($this->className, 'login');
    }

    /**
     * Store request for login
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeLogin(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return redirect(route('dashboard.index'))->with('success', 'Berhasil Masuk');
        }

        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ]);
    }

    /**
     * Display page register
     *
     * @return -
     */
    public function indexRegister()
    {
        $roles = MRoleUser::pluck('name', 'id');
        $semester = MSemester::
            orderBy('name', 'ASC')
            ->pluck('name', 'id')
            ->filter();
        
        return $this->getViewPage($this->className, 'register', compact('roles', 'semester'));
    }

    /**
     * Store request for register
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeRegister(Request $request)
    {
        // Custom validation unique user with soft delete
        Validator::extend('unique_user', function($attribute, $value, $parameters)
        {
            $data = User::where($attribute, $value)->get();
            if (count($data) > 0)
                return false;
            
            return true;
        });

        $validator = Validator::make($request->all(), [
            'no_id' => 'required|unique_user',
            'name' => 'required',
            'email' => 'required|email|unique_user',
            'password_confirmation' => 'required',
            'password' => 'required|confirmed',
            'phone' => 'nullable|numeric|unique_user',
            'semester' => 'required'
        ], [
            'no_id.unique_user' => 'NIM yang dimasukkan sudah terdaftar!',
            'no_id.required' => 'NIM tidak boleh kosong!',
            'name.required' => 'Nama tidak boleh kosong!',
            'email.required' => 'Email tidak boleh kosong!',
            'email.email' => 'Format Email tidak sesuai!',
            'email.unique_user' => 'Email yang dimasukkan sudah terdaftar!',
            'password.required' => 'Password tidak boleh kosong!',
            'password.confirmed' => 'Password dan Konfirmasi Password tidak sesuai!',
            'password_confirmation.required' => 'Konfirmasi Password tidak boleh kosong!',
            'phone.numeric' => 'No. HP/Telepon yang dimasukkan harus angka!',
            'phone.unique_user' => 'No. HP/Telepon yang dimasukkan sudah terdaftar!',
            'semester.required' => 'Semester tidak boleh kosong!'
        ]);
        $validator->validate();

        // Begin open database transaction
        DB::beginTransaction();

        try {
            $roleMahasiswa = '3';

            $user = new User;
            $user->no_id = $request->no_id;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->phone = $request->phone;
            $user->address = $request->address;
            $user->role_id = $roleMahasiswa; // Role Mahasiswa
            $user->save();

            $data = new Profile;
            $data->user_id = $user->id;
            $data->semester_id = $request->semester;
            $data->role_current_id = $roleMahasiswa; // Role Mahasiswa
            $data->save();

            DB::commit();

            // Start to login
            $credentials = $request->only('email', 'password');
            if (Auth::attempt($credentials)) {
                $request->session()->regenerate();
    
                return redirect(route('profile.indexStarted'));
            }
            
        } catch (\Exception $e) {
            DB::rollback();

            return redirect(route('auth.indexRegister'))->with('error', 'Terjadi kesalahan dalam membuat akun. Silahkan coba beberapa saat lagi..');
        }
    }

    /**
     * Store request for logout
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeLogout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect(route('auth.indexLogin'))->with('success', 'Berhasil Keluar');
    }

    /**
     * Show index page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $titlePage = 'Data Pengguna';

        return $this->getViewPage($this->pageDefault, 'index', compact('titlePage'), [
            'pathHeader' => 'pages/' . $this->className . '/headerIndex',
            'urlData' => route('user.dataIndex'),
                'headerColumns' => [
                    ['name' => 'No'],
                    ['name' => 'NIP/NIM'],
                    ['name' => 'Nama Pengguna'],
                    ['name' => 'Email'],
                    ['name' => 'No. Telp/HP'],
                    ['name' => 'Peran'],
                    ['name' => 'Aksi'],
                ],
                'dataColumns' => [
                    ['data' => 'DT_RowIndex', 'width' => '3%', 'className' => 'text-center'],
                    ['data' => 'col_no_id', 'className' => 'text-center'],
                    ['data' => 'name'],
                    ['data' => 'email'],
                    ['data' => 'col_phone', 'className' => 'text-center'],
                    ['data' => 'role.name', 'className' => 'text-center'],
                    ['data' => 'action', 'orderable' => false, 'searchable' => false, 'className' => 'text-center'],
                ]]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = MRoleUser::pluck('name', 'id');
        $semester = MSemester::
            orderBy('name', 'ASC')
            ->pluck('name', 'id')
            ->filter();

        return $this->getViewPage($this->pageDefault, 'formData', [
            'titlePage' => 'Buat Data Pengguna',
            'urlComponent' => 'formData.user',
            'formUrl' => route('user.store'),
            'options' => [
                'optRole' => $roles,
                'optSemester' => $semester
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'no_id' => 'required',
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'phone' => 'nullable|numeric|unique:users'
        ]);

        $roleMhs = '3';
        if ($request->role == $roleMhs) {
            $validatedData = $request->validate([
                'semester' => 'required',
            ]);
        }

        $passwordActive = $request->passwordActive;
        if ($passwordActive == 'on') {
            $validatedData = $request->validate([
                'password' => 'required|confirmed',
            ]);
        } else {
            $request->password = $this->passwordDefault;
        }

        DB::transaction(function() use ($request, $roleMhs) {
            $user = new User;
            $user->no_id = trim($request->no_id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->phone = $request->phone;
            $user->address = $request->address;
            $user->role_id = $request->role;
            $user->save();

            if ($request->role == $roleMhs) {
                $data = new Profile;
                $data->user_id = $user->id;
                $data->semester_id = $request->semester;
                $data->role_current_id = $request->role;
                $data->save();
            }
        });

        return redirect()->route('user.index')->with('success', 'Berhasil Menyimpan Data!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function storeResetPassword($id)
    {
        $data = User::find($id);
        $data->password = Hash::make($this->passwordDefault);
        $data->save();

        return redirect()->back()->with('success', 'Berhasil Me-reset Kata Sandi Pengguna ' . $data->name . '!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function storeChangePassword(Request $request)
    {
        $validatedData = $request->validate([
            'oldPassword' => 'required',
            'password' => 'required|confirmed',
        ]);

        $data = User::find(Auth::user()->id);
        if (! Hash::check($request->oldPassword, $data->password)) {
            return redirect()->back()->with('error', 'Kata Sandi Lama Yang Anda Masukkan Salah! Silahkan Coba Lagi..');
        }

        $data->password = Hash::make($request->password);
        $data->save();

        return redirect()->back()->with('success', 'Berhasil Merubah Kata Sandi!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::with(['role', 'profile'])->find($id);

        $roles = MRoleUser::pluck('name', 'id');
        $semester = MSemester::
            orderBy('name', 'ASC')
            ->pluck('name', 'id')
            ->filter();

        return $this->getViewPage($this->pageDefault, 'formData', [
            'titlePage' => 'Ubah Data Pengguna',
            'urlComponent' => 'formData.user',
            'formUrl' => route('user.update', $id),
            'formMethod' => 'PUT',
            'options' => [
                'optRole' => $roles,
                'optSemester' => $semester
            ],
            'data' => $data,
        ]);
    }

    /**
     * Update a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'no_id' => 'required',
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'nullable|numeric'
        ]);

        $roleMhs = '3';
        if ($request->role == $roleMhs) {
            $validatedData = $request->validate([
                'semester' => 'required',
            ]);
        }

        // Get data user
        $user = User::find($id);

        // Check matching value email
        if ($user->email != $request->email)
            $validatedData = $request->validate([
                'email' => 'unique:users'
            ]);

        // Check matching value phone
        if ($user->phone != $request->phone)
            $validatedData = $request->validate([
                'phone' => 'unique:users'
            ]);

        DB::transaction(function() use ($id, $user, $request, $roleMhs) {
            $user->no_id = $request->no_id;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->address = $request->address;
            $user->role_id = $request->role;
            $user->save();

            if ($request->role == $roleMhs) {
                $update = Profile::where('user_id', $user->id)->first();
                if ($update != null) {
                    $update->user_id = $user->id;
                    $update->semester_id = $request->semester;
                    $update->role_current_id = $request->role;
                    $update->save();
                } else {
                    $insert = new Profile;
                    $insert->user_id = $user->id;
                    $insert->semester_id = $request->semester;
                    $insert->role_current_id = $request->role;
                    $insert->save();
                }
            }
        });

        return redirect()->route('user.index')->with('success', 'Berhasil Menyimpan Data!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $data = User::destroy($id);

        return redirect()->back()->with('success', 'Berhasil Menghapus Data!');
    }

    /**
     * Get data index.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function dataIndex(Request $request)
    {
        if (! $request->ajax())
            return;

        $data = User::
            with(['role'])
            ->where('email', '!=', 'superman@app.com')
            ->orderBy('name', 'ASC')
            ->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('col_no_id', function($row) {
                $value = $row->no_id;
                return $value != null ? $value : '-';
            })
            ->addColumn('col_phone', function($row) {
                $value = $row->phone;
                return $value != null ? $value : '-';
            })
            ->addColumn('action', function($row) {
                $action = 
                    '<a href="' . route('user.edit', $row->id) . '" class="btn btn-warning btn-sm">Ubah</a>' .
                    '<button type="button" data-text-confirm="Yakin ingin menghapus data ini?" data-method="DELETE" data-url="' . route('user.destroy', $row->id) . '" class="btn-row-submit btn btn-danger btn-sm ml-2">Hapus</button>' .
                    '<button type="button" data-text-confirm="Yakin ingin me-reset kata sandi data ini?" data-method="POST" data-url="' . route('user.resetPassword', $row->id) . '" class="btn-row-submit btn btn-info btn-sm ml-2">Reset Kata Sandi</button>';
                return $action;
            })
            ->make(true);
    }
}