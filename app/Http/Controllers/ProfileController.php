<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;
use Auth;
use File;

use App\Models\User;
use App\Models\Profile;
use App\Models\UserActivity;
use App\Models\MSemester;

class ProfileController extends Controller
{
    private $className;

    /**
     * Constructor.
     *
     * @return -
     */
    public function __construct()
    {
        $this->className = $this->getClassName(get_class($this));
    }

    /**
     * Display a listing of the resource.
     *
     * @return -
     */
    public function index()
    {

    }

    /**
     * -
     *
     * @return -
     */
    public function indexSetting()
    {
        $data = Auth::user();
        $semester = MSemester::
            orderBy('name', 'ASC')
            ->pluck('name', 'id')
            ->filter();

        return $this->getViewPage($this->pageDefault, 'formData', [
            'titlePage' => 'Ubah Data Pengguna',
            'urlComponent' => 'formData.profile',
            'formUrl' => route('profile.storeSetting'),
            'formMethod' => 'PUT',
            'options' => [
                'optSemester' => $semester
            ],
            'data' => $data,
        ]);
    }

    /**
     * -
     *
     * @return -
     */
    public function storeSetting(Request $request)
    {
        $validatedData = $request->validate([
            'photo' => 'image|mimes:jpeg,png,jpg,svg|max:5000',
            'no_id' => 'required',
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'semester' => 'required',
            'generation' => 'nullable|min:4|max:4'
        ]);

        // Get data user logged in
        $user = Auth::user();

        // Check matching value email
        if ($user->email != $request->email)
            $validatedData = $request->validate([
                'email' => 'unique:users'
            ]);

        // Check matching value phone
        if ($user->phone != $request->phone)
            $validatedData = $request->validate([
                'phone' => 'unique:users'
            ]);

        $roleMhs = '3';
        DB::transaction(function() use ($user, $request, $roleMhs) {
            $filePhoto = null;
            if ($request->photo != null) {
                $filePhoto = 'photo-' . time() . '.' . $request->photo->extension();
            }

            $user->photo = $filePhoto;
            $user->no_id = $request->no_id;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->address = $request->address;
            $user->generation = $request->generation;
            $user->role_id = $user->role_id;
            $user->save();

            $update = Profile::where('user_id', $user->id)
                ->where('semester_id', $request->semester)
                ->first();

            if ($update != null) {
                $update->user_id = $user->id;
                $update->semester_id = $request->semester;
                $update->role_current_id = $user->role_id;
                $update->save();
            } else {
                $insert = new Profile;
                $insert->user_id = $user->id;
                $insert->semester_id = $request->semester;
                $insert->role_current_id = $user->role_id;
                $insert->save();
            }

            if ($filePhoto != null) {
                // Remove old photo
                $filePath = public_path('uploads/photo_profile/' . $user->photo);
                if (file_exists($filePath))
                    unlink($filePath);

                // Upload new photo
                $request->photo->move(public_path('uploads/photo_profile'), $filePhoto);
            }
        });

        return redirect()->back()->with('success', 'Data Profile Berhasil Diubah!');
    }

    /**
     * -
     *
     * @return -
     */
    public function indexStarted()
    {
        return $this->getViewPage($this->className, 'started.form');
    }

    /**
     * -
     *
     * @return -
     */
    public function storeStarted(Request $request)
    {
        $validatedData = $request->validate([
            'generation' => 'nullable|min:4|max:4',
            'phone' => 'nullable|numeric'
        ]);

        $userIdLogin = Auth::user()->id;
        $profileIdLogin = isset(Auth::user()->profile) ? Auth::user()->profile->id : null;

        // Merge to single array
        $activity = [];
        for ($index = 0; $index < count($request->name_activity); $index++) {
            $nameActivity = $request->name_activity[$index];
            $fileDocument = null;

            if (isset($request->file_document[$index])) {
                $fileDocument = time() . '-' . Str::random(40) . '.' . $request->file_document[$index]->extension();
                $request->file_document[$index]->move(public_path('uploads/file_document'), $fileDocument);
            }

            if ($nameActivity == null && $fileDocument == null)
                continue;

            array_push($activity, [
                'user_id' => $userIdLogin,
                'name' => $nameActivity,
                'file_document' => $fileDocument,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }

        // Begin open database transaction
        DB::beginTransaction();
        try {
            $user = User::find($userIdLogin);
            $user->generation = $request->generation;
            $user->phone = $request->phone;
            $user->save();

            // Bulk store user activity
            UserActivity::insert($activity);

            // Commit database transaction
            DB::commit();

            return redirect()->route('dashboard.index')->with('success', 'Berhasil menyimpan data.');
        } catch (\Exception $e) {
            // Rollback database transaction
            DB::rollback();

            foreach ($activity as $row) {
                $fileName = $row['file_document'];
                if ($fileName == null)
                    continue;

                $filePath = 'uploads/file_document/' . $fileName;
                if (File::exists(public_path($filePath))){
                    File::delete(public_path($filePath));
                }
            }

            return redirect()->route('profile.indexSetting')->with('error', 'Terjadi kesalahan dalam menyimpan data. Silahkan coba beberapa saat lagi..');
        }
    }
}
