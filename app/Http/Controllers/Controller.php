<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Auth;

use App\Models\Profile;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $pageDefault = '_default';
    protected $passwordDefault = 'uin12345';

    protected function getClassName($className)
    {
        $arrClass = explode('\\', $className);

        return 
            strtolower(str_replace('Controller', '', end($arrClass)));
    }

    protected function getViewPage($className, $pathView, $compactData = [], $arrData = [])
    {
       return view('pages.' . $className . '.' . $pathView, $compactData, $arrData);
    }
}
