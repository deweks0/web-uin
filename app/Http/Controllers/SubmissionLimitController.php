<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Auth;
use DataTables;

use App\Models\MSetting;
use App\Models\MRoleUser;
use App\Models\ProfileActivity;
use App\Models\SubmissionLimitApproval;

class SubmissionLimitController extends Controller
{
    private $className;

    /**
     * Constructor.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->className = $this->getClassName(get_class($this));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titlePage = 'Daftar Pengajuan Batas Persetujuan';

        return $this->getViewPage($this->pageDefault, 'index', compact('titlePage'), [
            'pathHeader' => 'pages/' . $this->className . '/headerIndex',
            'urlData' => route('submissionLimit.dataIndex'),
            'headerColumns' => [
                ['name' => 'No'],
                ['name' => 'Digunakan Tanggal'],
                ['name' => 'Batas Approval'],
                // ['name' => 'Sisa Approval'],
                ['name' => 'Status Pengajuan'],
                ['name' => 'Tanggal Pengajuan']
            ],
            'dataColumns' => [
                ['data' => 'DT_RowIndex', 'width' => '3%', 'className' => 'text-center'],
                ['data' => 'col_used_date', 'className' => 'text-center'],
                ['data' => 'limit_approval', 'className' => 'text-center'],
                // ['data' => 'col_rest_approval', 'className' => 'text-center'],
                ['data' => 'col_status_submission', 'className' => 'text-center'],
                ['data' => 'col_created_at', 'className' => 'text-center']
            ]]);
    }

    public function indexReview()
    {
        $titlePage = 'Daftar Pengajuan Batas Persetujuan';

        return $this->getViewPage($this->pageDefault, 'index', compact('titlePage'), [
            'urlData' => route('submissionLimit.dataReview'),
            'headerColumns' => [
                ['name' => 'No'],
                ['name' => 'Nama Dosen'],
                ['name' => 'Digunakan Tanggal'],
                ['name' => 'Batas Approval'],
                ['name' => 'Status Pengajuan'],
                ['name' => 'Tanggal Pengajuan'],
                ['name' => 'Aksi'],
            ],
            'dataColumns' => [
                ['data' => 'DT_RowIndex', 'width' => '3%', 'className' => 'text-center'],
                ['data' => 'user.name'],
                ['data' => 'col_used_date', 'className' => 'text-center'],
                ['data' => 'limit_approval', 'className' => 'text-center'],
                ['data' => 'col_status_submission', 'className' => 'text-center'],
                ['data' => 'col_created_at', 'className' => 'text-center'],
                ['data' => 'action', 'orderable' => false, 'searchable' => false, 'className' => 'text-center']
            ]]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'limitApproval' => 'required|numeric',
            'usedDate' => 'required',
        ]);

        // Begin open database transaction
        DB::beginTransaction();

        try {
            // Store data
            $data = new SubmissionLimitApproval;
            $data->user_id = Auth::user()->id;
            $data->limit_approval = $request->limitApproval;
            $data->used_date = $request->usedDate;
            $data->save();

            // Get data role Superuser
            $getRoleSU = MRoleUser::where('code', 'SU')->first();

            // Send notification
            sendNotification([
                'role_id' => $getRoleSU->id,
                'title' => 'Pengajuan Batas Persetujuan Dosen',
                'message' => 'Terdapat pengajuan untuk penambahan batas persetujuan dosen. Diajukan oleh ' . Auth::user()->name . ' pada Tanggal ' . Carbon::now()->isoFormat('dddd, D MMMM Y'),
                'url_direct' => route('submissionLimit.indexReview')
            ]);

            DB::commit();

            return redirect()->back()->with('success', 'Berhasil Membuat Pengajuan!');
        } catch (\Exception $e) {
            DB::rollback();

            return redirect()->back()->with('error', 'Terjadi kesalahan dalam membuat pengajuan. Silahkan coba beberapa saat lagi..');
        }
    }

    public function dataIndex(Request $request)
    {
        if (! $request->ajax())
            return;

        // Init variable
        $userIdLogin = Auth::user()->id;

        // Get data submission limit
        $data = SubmissionLimitApproval::
            with('user')
            ->where('user_id', Auth::user()->id)
            ->orderBy('created_at', 'DESC')
            ->get();

        // Get data setting
        $getSetting = MSetting::first();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('col_used_date', function($row) {
                return $row->used_date->isoFormat('dddd, D MMMM Y');
            })
            // ->addColumn('col_rest_approval', function($row) use ($getSetting, $userIdLogin) {
            //     // Init variable
            //     $subLimitApproval = $row->limit_approval;
            //     $statusApprove = $row->status_approve;

            //     // If status approve equals to Waiting and Rejected
            //     if ($statusApprove == 0 || $statusApprove == 2) 
            //         return '-';

            //     if ($getSetting == null)
            //         return '∞';

            //     // Get profile activity approved by Dosen
            //     $countDataApproved = ProfileActivity::
            //         where('approved_by', $userIdLogin)
            //         ->whereDate('approved_at', $row->used_date)
            //         ->count();

            //     // Get setting limit approval
            //     $limitApprovalSetting = $getSetting->limit_approval;

            //     // Calculation from Limit Approval in Setting
            //     // With Data Approval User
            //     $calLimitApprovalSetting = $limitApprovalSetting - $countDataApproved;

            //     // Check if Calculation from Limit Approval in Setting
            //     // Is less than 1
            //     if ($calLimitApprovalSetting > 0)
            //         return $subLimitApproval;

            //     return $subLimitApproval - ($calLimitApprovalSetting * -1);
            // })
            ->addColumn('col_status_submission', function($row) {
                $value = $row->status_approve;
                switch ($value) {
                    case 0 :
                        return 'Menunggu Konfirmasi';
                        break;
                    case 1 :
                        return 'Disetujui';
                        break;
                    case 2 :
                        return 'Ditolak';
                        break;
                }
            })
            ->addColumn('col_created_at', function($row) {
                return $row->created_at->isoFormat('dddd, D MMMM Y');
            })
            ->make(true);
    }

    public function dataReview(Request $request)
    {
        if (! $request->ajax())
            return;

        // Get data submission limit
        $data = SubmissionLimitApproval::
            with('user')
            ->where('status_approve', 0) // Where status approve is waiting
            ->orderBy('created_at', 'DESC')
            ->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('col_used_date', function($row) {
                return $row->used_date->isoFormat('dddd, D MMMM Y');
            })
            ->addColumn('col_status_submission', function($row) {
                $value = $row->status_approve;
                
                if ($value == 0)
                    return 'Menunggu Konfirmasi';

                if ($value == 1)
                    return 'Disetujui';
                
                if ($value == 2)
                    return 'Ditolak';
            })
            ->addColumn('col_created_at', function($row) {
                return $row->created_at->isoFormat('dddd, D MMMM Y');
            })
            ->addColumn('action', function($row) {
                $action = 
                    '<button type="button" data-text-confirm="Yakin ingin menyetujui pengajuan ini?" data-method="POST" data-url="' . route('submissionLimit.storeApproveData', $row->id) . '" class="btn-row-submit btn btn-primary btn-sm ml-2">Setujui</button>' .
                    '<button type="button" data-text-confirm="Yakin ingin menolak pengajuan ini?" data-method="POST" data-url="' . route('submissionLimit.storeRejectData', $row->id) . '" class="btn-row-submit btn btn-danger btn-sm ml-2">Tolak</button>';
                return $action;
            })
            ->make(true);
    }

    private function _updateStatus($submission_id, $status)
    {
        // Init variable
        $userIdLogin = Auth::user()->id;

        // Get data Submission Limit
        $data = SubmissionLimitApproval::find($submission_id);

        // Update data
        $data->approved_by = $userIdLogin;
        $data->approved_at = date('Y-m-d H:i:s');

        // Update status
        if ($status == 'VERIFY')
            $data->status_approve = 1; // Status approved
        else if ($status == 'REJECT')
            $data->status_approve = 2; // Status rejected

        // Save stored data
        $response = $data->save();

        // Send notification
        if ($response) {
            $titleNotif = $status == 'VERIFY' ?
                'Pengajuan Batas Persetujuan Disetujui' :
                'Pengajuan Batas Persetujuan Ditolak';
            $messageNotif = $status == 'VERIFY' ?
                'Pengajuan Batas Persetujuan Telah Disetujui' : 
                'Pengajuan Batas Persetujuan Telah Ditolak';

            sendNotification([
                'user_id' => $data->user_id,
                'title' => $titleNotif,
                'message' => $messageNotif,
                'url_direct' => route('submissionLimit.index')
            ]);
        }

        if (! $response) 
            return false;

        return true;
    }

    public function storeApproveData($submission_id)
    {
        $codeResponse = 'success';
        $messageResponse = 'Berhasil menyetujui data pengajuan';

        $response = $this->_updateStatus($submission_id, 'VERIFY');
        if (! $response) {
            $codeResponse = 'error';
            $messageResponse = 'Terjadi kesalahan dalam menyimpan data. Silahkan coba beberapa saat lagi..';
        }

        return redirect()->route('submissionLimit.indexReview')->with($codeResponse, $messageResponse);
    }

    public function storeRejectData($submission_id)
    {
        $codeResponse = 'success';
        $messageResponse = 'Berhasil menolak data pengajuan';

        $response = $this->_updateStatus($submission_id, 'REJECT');
        if (! $response) {
            $codeResponse = 'error';
            $messageResponse = 'Terjadi kesalahan dalam menyimpan data. Silahkan coba beberapa saat lagi..';
        }

        return redirect()->route('submissionLimit.indexReview')->with($codeResponse, $messageResponse);
    }
}