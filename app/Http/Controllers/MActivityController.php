<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;

use App\Models\MParentActivity;
use App\Models\MActivity;

class MActivityController extends Controller
{
    private $className;

    /**
     * Constructor.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->className = $this->getClassName(get_class($this));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titlePage = 'Daftar Master Kegiatan';

        return $this->getViewPage($this->pageDefault, 'index', compact('titlePage'), [
                'pathHeader' => 'pages/' . $this->className . '/headerIndex',
                'urlData' => route('mActivity.dataIndex'),
                'headerColumns' => [
                    ['name' => 'No'],
                    ['name' => 'Tipe Kegiatan'],
                    ['name' => 'Nama Kegiatan'],
                    ['name' => 'Aksi'],
                ],
                'dataColumns' => [
                    ['data' => 'DT_RowIndex', 'width' => '3%', 'className' => 'text-center'],
                    ['data' => 'parent_activity.name'],
                    ['data' => 'name'],
                    ['data' => 'action', 'orderable' => false, 'searchable' => false, 'className' => 'text-center'],
                ]]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $optParentAct = MParentActivity::orderBy('name', 'ASC')
            ->pluck('name', 'code')
            ->filter();

        return $this->getViewPage($this->pageDefault, 'formData', [
            'titlePage' => 'Tambah Master Kegiatan',
            'urlComponent' => 'formData.mActivity',
            'formUrl' => route('mActivity.store'),
            'options' => [
                'optParentAct' => $optParentAct,
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'parentActivity' => 'required',
            'name' => 'required'
        ]);

        $data = new MActivity;
        $data->code = $request->parentActivity;
        $data->name = $request->name;
        $data->save();

        return redirect()->route('mActivity.index')->with('success', 'Berhasil Menyimpan Data!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = MActivity::find($id);

        $optParentAct = MParentActivity::orderBy('name', 'ASC')
            ->pluck('name', 'code')
            ->filter();

        return $this->getViewPage($this->pageDefault, 'formData', [
            'titlePage' => 'Ubah Master Kegiatan',
            'urlComponent' => 'formData.mActivity',
            'formUrl' => route('mActivity.update', $id),
            'formMethod' => 'PUT',
            'options' => [
                'optParentAct' => $optParentAct,
            ],
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'parentActivity' => 'required',
            'name' => 'required'
        ]);

        $data = MActivity::find($id);
        $data->code = $request->parentActivity;
        $data->name = $request->name;
        $data->save();

        return redirect()->route('mActivity.index')->with('success', 'Berhasil Menyimpan Data!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = MActivity::destroy($id);

        return redirect()->back()->with('success', 'Berhasil Menghapus Data!');
    }

    public function dataIndex(Request $request)
    {
        if (! $request->ajax())
            return;

        $data = MActivity::
            with('parentActivity')
            ->orderBy('code', 'ASC')
            ->orderBy('name', 'ASC')
            ->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row) {
                $action = 
                    '<a href="' . route('mActivity.edit', $row->id) . '" class="btn btn-warning btn-sm">Ubah</a>' .
                    '<button type="button" data-text-confirm="Yakin ingin menghapus data ini?" data-method="DELETE" data-url="' . route('mActivity.destroy', $row->id) . '" class="btn-row-submit btn btn-danger btn-sm ml-2">Hapus</button>';
                return $action;
            })
            ->make(true);
    }
}
