<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use DataTables;

use App\Models\MSemester;
use App\Models\MParentActivity;
use App\Models\SemesterMission;

class SemesterMissionController extends Controller
{
    private $className;

    /**
     * Constructor.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->className = $this->getClassName(get_class($this));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titlePage = 'Daftar Misi Target Skor Per-Semester';
        
        return $this->getViewPage($this->pageDefault, 'index', compact('titlePage'), [
            'pathHeader' => 'pages/' . $this->className . '/headerIndex',
            'urlData' => route('semesterMission.dataIndex'),
            'headerColumns' => [
                ['name' => 'No'],
                ['name' => 'Nama Semester'],
                ['name' => 'Nama Bidang'],
                ['name' => 'Target Total Skor'],
                ['name' => 'Aksi'],
            ],
            'dataColumns' => [
                ['data' => 'DT_RowIndex', 'width' => '3%', 'className' => 'text-center'],
                ['data' => 'semester.name'],
                ['data' => 'parent_activity.name'],
                ['data' => 'target_total_score'],
                ['data' => 'action', 'orderable' => false, 'searchable' => false],
            ]]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $optSemester = MSemester::orderBy('name', 'ASC')
            ->pluck('name', 'id')
            ->filter();

        $optParentAct = MParentActivity::orderBy('name', 'ASC')
            ->pluck('name', 'code')
            ->filter();

        return $this->getViewPage($this->pageDefault, 'formData', [
            'titlePage' => 'Buat Misi Target Total Skor Semester',
            'urlComponent' => 'formData.semesterMission',
            'formUrl' => route('semesterMission.store'),
            'options' => [
                'optSemester' => $optSemester,
                'optParentAct' => $optParentAct,
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validatedData = $request->validate([
            'semester' => 'required',
            'type_activity' => 'required',
            'target_total_score' => 'required|numeric',
        ]);

        // Check exist data
        if ($this->onCheckExistData($request)) {
            return redirect()->back()->with('error', 'Data semester dan bidang yang dipilih sudah tersedia!');
        }

        $data = new SemesterMission;
        $data->semester_id = $request->semester;
        $data->parent_activity_id = $request->type_activity;
        $data->target_total_score = $request->target_total_score;
        $data->save();

        return redirect()->route('semesterMission.index')->with('success', 'Berhasil Menyimpan Data!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data = SemesterMission::
            with(['semester', 'parentActivity'])
            ->find($id);

        $optSemester = MSemester::orderBy('name', 'ASC')
            ->pluck('name', 'id')
            ->filter();

        $optParentAct = MParentActivity::orderBy('name', 'ASC')
            ->pluck('name', 'code')
            ->filter();

        return $this->getViewPage($this->pageDefault, 'formData', [
            'titlePage' => 'Buat Misi Target Total Skor Semester',
            'urlComponent' => 'formData.semesterMission',
            'formUrl' => route('semesterMission.update', $id),
            'formMethod' => 'PUT',
            'options' => [
                'optSemester' => $optSemester,
                'optParentAct' => $optParentAct,
            ],
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $validatedData = $request->validate([
            'semester' => 'required',
            'type_activity' => 'required',
            'target_total_score' => 'required|numeric',
        ]);

        // Get data by request id
        $data = SemesterMission::find($id);

        // Check matches old id and id in database
        if ($request->semester != $data->semester_id ||
            $request->type_activity != $data->parent_activity_id) {

            // Check exist data
            if ($this->onCheckExistData($request)) {
                return redirect()->back()->with('error', 'Data semester dan bidang yang dipilih sudah tersedia!');
            }
        }

        $data->semester_id = $request->semester;
        $data->parent_activity_id = $request->type_activity;
        $data->target_total_score = $request->target_total_score;
        $data->save();

        return redirect()->route('semesterMission.index')->with('success', 'Berhasil Menyimpan Data!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $data = SemesterMission::destroy($id);

        return redirect()->back()->with('success', 'Berhasil Menghapus Data!');
    }

    private function onCheckExistData(Request $request)
    {
        $checkData = SemesterMission::
            where('semester_id', $request->semester)
            ->where('parent_activity_id', $request->type_activity)
            ->first();

        if ($checkData != null) {
            return true;
        }

        return false;
    }

    /**
     * Get data index.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function dataIndex(Request $request)
    {
        if (! $request->ajax())
            return;

        $data = SemesterMission::
            with(['semester', 'parentActivity'])
            ->orderBy('semester_id', 'ASC')
            ->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row) {
                $action = 
                    '<a href="' . route('semesterMission.edit', $row->id) . '" class="btn btn-warning btn-sm">Ubah</a>' .
                    '<button type="button" data-text-confirm="Yakin ingin menghapus data ini?" data-method="DELETE" data-url="' . route('semesterMission.destroy', $row->id) . '" class="btn-row-submit btn btn-danger btn-sm ml-2">Hapus</button>';;
                return $action;
            })
            ->make(true);
    }
}
