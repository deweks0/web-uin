<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use PDF;

use App\Models\User;
use App\Models\ProfileActivity;

class CollegerController extends Controller
{
    private $className;

    /**
     * Constructor.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->className = $this->getClassName(get_class($this));
    }

    /**
     * -
     *
     * @param -
     * @return -
     */
    public function index()
    {
        $titlePage = 'Daftar Mahasiswa';

        return $this->getViewPage($this->pageDefault, 'index', compact('titlePage'), [
            'urlData' => route('colleger.dataIndex'),
            'headerColumns' => [
                ['name' => 'No'],
                ['name' => 'NIM'],
                ['name' => 'Nama'],
                ['name' => 'Email'],
                ['name' => 'No. Telp/HP'],
                ['name' => 'Angkatan'],
                ['name' => 'Total Skor']
            ],
            'dataColumns' => [
                ['data' => 'DT_RowIndex', 'width' => '3%', 'className' => 'text-center'],
                ['data' => 'no_id', 'className' => 'text-center'],
                ['data' => 'col_name'],
                ['data' => 'email'],
                ['data' => 'phone', 'className' => 'text-center'],
                ['data' => 'generation', 'className' => 'text-center'],
                ['data' => 'profile_score', 'className' => 'text-center']
            ]]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = User::with(['role', 'activity', 'profile.profileActivities.typeActivity.activity'])
            ->where('id', $id)
            ->first();

        $collectPoint = [
            'organisasi' => 0,
            'minat_bakat' => 0,
            'pengabdian' => 0,
            'kewirausahaan' => 0,
            'keilmuan' => 0
        ];

        $profileActivities = $data->profile->profileActivities;
        foreach ($profileActivities as $row) {
            if ($row->status != 'VERIFY')
                continue;

            $totalScore = $row->total_score;
            $codeActivity = $row->typeActivity->activity->code;

            if ($codeActivity == 'PENALARAN_KEILMUAN')
                $collectPoint['keilmuan'] += $totalScore;

            if ($codeActivity == 'ORGANISASI_KEPEMIMPINAN')
                $collectPoint['organisasi'] += $totalScore;

            if ($codeActivity == 'MINAT_BAKAT')
                $collectPoint['minat_bakat'] += $totalScore;

            if ($codeActivity == 'PENGABDIAN_MASYARAKAT')
                $collectPoint['pengabdian'] += $totalScore;

            if ($codeActivity == 'KEWIRAUSAHAAN')
                $collectPoint['kewirausahaan'] += $totalScore;
        }

        return $this->getViewPage($this->className, 'show', [
                'user_id' => $id,
                'titlePage' => 'Profil Mahasiswa',
                'data' => $data,
                'collectPoint' => $collectPoint,
                'urlData' => route('colleger.dataPortofolio', $id),
                'headerColumns' => [
                    ['name' => 'No'],
                    ['name' => 'Nama Kegiatan'],
                    ['name' => 'Kategori'],
                    ['name' => 'Tingkat'],
                    ['name' => 'Peran'],
                    ['name' => 'Skor'],
                    ['name' => 'Status'],
                ],
                'dataColumns' => [
                    ['data' => 'DT_RowIndex', 'width' => '3%', 'className' => 'text-center'],
                    ['data' => 'name'],
                    ['data' => 'type_activity.activity.name', 'className' => 'text-center'],
                    ['data' => 'col_level', 'className' => 'text-center'],
                    ['data' => 'col_role_activity', 'className' => 'text-center'],
                    ['data' => 'total_score', 'className' => 'text-center'],
                    ['data' => 'status', 'className' => 'text-center'],
                ]
            ]);
    }

    /**
     * Get data portofolio.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function dataPortofolio(Request $request, $userId)
    {
        if ($request->ajax()) {
            $profileClause = function($query) use ($userId) {
                $query->where('user_id', $userId);
            };

            $data = ProfileActivity::
                with([
                    'typeActivity.activity.parentActivity',
                    'typeActivity.level',
                    'typeActivity.roleActivity',
                    'profileUser'])
                ->whereHas('profileUser', $profileClause)
                ->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('col_level', function($row) {
                    $value = isset($row->typeActivity->level) ? $row->typeActivity->level : null;
                    if ($value == null)
                        return '-';

                    return $value->name;
                })
                ->addColumn('col_role_activity', function($row) {
                    $value = isset($row->typeActivity->roleActivity) ? $row->typeActivity->roleActivity : null;
                    if ($value == null)
                        return '-';

                    return $value->name;
                })
                ->addColumn('impl_date', function($row) {
                    return formatDate($row->implementation_date);
                })
                ->addColumn('col_created_at', function($row) {
                    return formatDate($row->implementation_date);
                })
                ->addColumn('action', function($row) {
                    $action = '<a href="' . route('portofolio.show', $row->id) . '" class="edit btn btn-warning btn-sm">Detail</a>';
                    return $action;
                })
                ->addColumn('col_file_document', function($row) {
                    return '<a href="' . asset('uploads/file_document/' . $row->file_document) . '" class="edit btn btn-link btn-sm">Unduh</a>';
                })
                ->editColumn('status', function($row) {
                    $value = '-';
                    switch ($row->status) {
                        case 'WAITING':
                            $value = 'Menunggu Verifikasi';
                            break;
                        case 'VERIFY':
                            $value = 'Terverifikasi';
                            break;
                        case 'REJECT':
                            $value = 'Ditolak';
                            break;
                    }
                    return $value;
                })
                ->rawColumns(['delete' => 'col_file_document','action' => 'action'])
                ->make(true);
        }
    }

    public function dataIndex(Request $request)
    {
        if (! $request->ajax())
            return;

        $data = User::with(['role', 'profile'])
            ->whereHas('role', function($query) {
                $query->where('code', 'MHS');
            })
            ->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('col_name', function($row) {
                return '<a href="' . route('colleger.show', $row->id) . '">' . $row->name . '</a>';
            })
            ->addColumn('profile_score', function($row) {
                return $row->profile->profile_score . ' point';
            })
            ->rawColumns(['action' => 'col_name'])
            ->make(true);
    }

    public function printPDF($id)
    {
        $user = User::with(['role', 'activity', 'profile.profileActivities.typeActivity.activity'])
            ->where('id', $id)
            ->first();
        if ($user == null)
            return redirect()->back('error', 'Data pengguna tidak ditemukan!');

        $portofolio = ProfileActivity::
            with([
                'typeActivity.activity.parentActivity',
                'typeActivity.level',
                'typeActivity.roleActivity',
                'profileUser'])
            ->whereHas('profileUser', function($query) use ($id) {
                $query->where('user_id', $id);
            })
            ->get();

        $collectPoint = [
            'organisasi' => 0,
            'minat_bakat' => 0,
            'pengabdian' => 0,
            'kewirausahaan' => 0,
            'keilmuan' => 0
        ];

        $profileActivities = $user->profile->profileActivities;
        foreach ($profileActivities as $row) {
            if ($row->status != 'VERIFY')
                continue;

            $totalScore = $row->total_score;
            $codeActivity = $row->typeActivity->activity->code;

            if ($codeActivity == 'PENALARAN_KEILMUAN')
                $collectPoint['keilmuan'] += $totalScore;

            if ($codeActivity == 'ORGANISASI_KEPEMIMPINAN')
                $collectPoint['organisasi'] += $totalScore;

            if ($codeActivity == 'MINAT_BAKAT')
                $collectPoint['minat_bakat'] += $totalScore;

            if ($codeActivity == 'PENGABDIAN_MASYARAKAT')
                $collectPoint['pengabdian'] += $totalScore;

            if ($codeActivity == 'KEWIRAUSAHAAN')
                $collectPoint['kewirausahaan'] += $totalScore;
        }

        $pdf = PDF::loadview('pages/' . $this->className . '/printPDF', [
            'user_id' => $id,
            'titlePage' => 'Profil Mahasiswa',
            'data' => $user,
            'collectPoint' => $collectPoint,
            'portofolio' => $portofolio
        ]);

    	return $pdf->download('laporan-mahasiswa-' . $user->name . '-' . date('Ymd-His') . '.pdf');
    }
}