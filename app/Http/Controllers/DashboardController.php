<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use Auth;

use App\Models\SemesterMission;
use App\Models\UserActivity;

class DashboardController extends Controller
{
    private $className;

    private const CODE_PENALARAN_KEILMUAN = 'PENALARAN_KEILMUAN';
    private const CODE_ORGANISASI_KEPEMIMPINAN = 'ORGANISASI_KEPEMIMPINAN';
    private const CODE_MINAT_BAKAT = 'MINAT_BAKAT';
    private const CODE_PENGABDIAN_MASYARAKAT = 'PENGABDIAN_MASYARAKAT';
    private const CODE_KEWIRAUSAHAAN = 'KEWIRAUSAHAAN';

    /**
     * Constructor.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->className = $this->getClassName(get_class($this));
    }

    /**
     * Display page dashboard
     *
     * @return -*/
    public function index()
    {
        $userRoleId = \Auth::user()->role_id;

        switch ($userRoleId) {
            case "3" :
                return $this->indexColleger();
                break;
            default :
                return $this->indexDefault();
                break;
        }
    }

    private function indexDefault()
    {
        return $this->getViewPage($this->className, 'index');
    }

    private function indexColleger()
    {
        $collectPoint = [
            self::CODE_PENALARAN_KEILMUAN => 0,
            self::CODE_ORGANISASI_KEPEMIMPINAN => 0,
            self::CODE_MINAT_BAKAT => 0,
            self::CODE_PENGABDIAN_MASYARAKAT => 0,
            self::CODE_KEWIRAUSAHAAN => 0
        ];

        $userData = \Auth::user();

        // Get user's profile activities
        $profileActivities = $userData->profile->profileActivities;
        foreach ($profileActivities as $row) {

            // Continue if status isn't VERIFY
            if ($row->status != 'VERIFY')
                continue;

            // Initial value data
            $totalScore = $row->total_score;
            $codeActivity = $row->typeActivity->activity->code;
            
            if ($codeActivity == self::CODE_PENALARAN_KEILMUAN)
                $collectPoint[self::CODE_PENALARAN_KEILMUAN] += $totalScore;

            if ($codeActivity == self::CODE_ORGANISASI_KEPEMIMPINAN)
                $collectPoint[self::CODE_ORGANISASI_KEPEMIMPINAN] += $totalScore;

            if ($codeActivity == self::CODE_MINAT_BAKAT)
                $collectPoint[self::CODE_MINAT_BAKAT] += $totalScore;

            if ($codeActivity == self::CODE_PENGABDIAN_MASYARAKAT)
                $collectPoint[self::CODE_PENGABDIAN_MASYARAKAT] += $totalScore;

            if ($codeActivity == self::CODE_KEWIRAUSAHAAN)
                $collectPoint[self::CODE_KEWIRAUSAHAAN] += $totalScore;

        }

        // Get semester's mission
        $userSemester = $userData->profile->semester_id;
        $missionDone = SemesterMission::
            with('parentActivity')
            ->where('semester_id', $userSemester)->get();

        foreach ($missionDone as $row) {
            $codeActivity = $row->parent_activity_id;

            $row->current_score = isset($collectPoint[$codeActivity]) ? $collectPoint[$codeActivity] : 0;
        }

        return $this->getViewPage($this->className, 'index', 
            compact('collectPoint', 'missionDone'), [
                'urlDataPrestasi' => route('dashboard.dataPrestasi'),
                'urlDataPortofolio' => route('colleger.dataPortofolio', Auth::user()->id),
                'dataColumnsPrestasi' => [
                    ['data' => 'DT_RowIndex', 'width' => '3%', 'className' => 'text-center'],
                    ['data' => 'name'],
                    ['data' => 'action', 'orderable' => false, 'searchable' => false, 'className' => 'text-center'],
                ],
                'dataColumnsPortofolio' => [
                    ['data' => 'DT_RowIndex', 'width' => '3%', 'className' => 'text-center'],
                    ['data' => 'type_activity.activity.parent_activity.name', 'className' => 'text-center'],
                    ['data' => 'type_activity.activity.name'],
                    ['data' => 'col_level', 'className' => 'text-center'],
                    ['data' => 'col_role_activity', 'className' => 'text-center'],
                    ['data' => 'name'],
                    ['data' => 'impl_date', 'className' => 'text-center'],
                    ['data' => 'col_file_document', 'className' => 'text-center'],
                    ['data' => 'status', 'className' => 'text-center'],
                    ['data' => 'col_created_at', 'className' => 'text-center']
                ]
            ]);
    }

    public function dataPrestasi(Request $request)
    {
        if (! $request->ajax())
            return;

        $data = UserActivity::
            where('user_id', Auth::user()->id)
            ->orderBy('name', 'DESC')
            ->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row) {
                $fileDocument = $row->file_document;
                if ($fileDocument == null)
                    return 'Tidak ada file';

                $action = 
                    '<a href="' . asset('uploads/file_document/' . $fileDocument) . '" class="btn btn-link btn-sm">Unduh</a>';
                return $action;
            })
            ->make(true);
    }
}