<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Models\Notification;

class NotificationController extends Controller
{
    public function getData(Request $request)
    {
        if (! $request->ajax()) 
            return false;

        $userIdLogin = Auth::user()->id;
        $data = Notification::
            where('user_id', $userIdLogin)
            ->where('status_view', '0')
            ->orderBy('created_at', 'DESC')
            ->get();

        return response()->json([
            'countNotif' => count($data),
            'data' => $data
        ]);
    }

    public function redirectData(Request $request)
    {
        $id = $request->id;
        $urlDirect = $request->url != null ? $request->url : route('dashboard.index');

        return redirect($urlDirect);
    }
}
