@extends('layouts.master')

@section('body')
<body class="hold-transition login-page">
  <div class="login-box">

    @yield('content')

  </div>
@endsection