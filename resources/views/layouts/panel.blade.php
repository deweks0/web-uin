@extends('layouts.master')

@section('body')

<!-- Modal Send Message -->
<div class="modal fade" id="modalSendMessage" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <form method="POST" action="{{ route('portofolio.sendMessage') }}" autocomplete="off">
      @csrf
      <input type="hidden" name="user" id="user">

      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Kirim Pesan <span id="sendTo"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label for="message">Pesan</label>
            <textarea name="message" class="form-control" id="message" name="message" placeholder="Pesan" rows="5"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
    </form>
    </div>
  </div>
</div>

<!-- Modal Change Password -->
<div class="modal fade" id="modalChangePassword" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <form method="POST" action="{{ route('user.changePassword') }}" autocomplete="off">
        @csrf
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Ubah Kata Sandi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label for="oldPassword">Kata Sandi Lama</label>
            <input type="password" class="form-control" id="oldPassword" name="oldPassword" placeholder="Kata Sandi Lama">
        </div>
        <div class="form-group">
            <label for="password">Kata Sandi Baru</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Kata Sandi Baru">
        </div>
        <div class="form-group">
            <label for="confPassword">Konfirmasi Kata Sandi Baru</label>
            <input type="password" class="form-control" id="confPassword" name="password_confirmation" placeholder="Konfirmasi Kata Sandi Baru">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
    </form>
    </div>
  </div>
</div>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>

            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="far fa-bell"></i>
                        <span class="badge badge-warning navbar-badge notif-count"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <span class="dropdown-header"><span class="notif-bar-count"></span> Notifikasi</span>
                        <div class="notif-content"></div>
                        <div class="dropdown-divider"></div>
                        <span class="dropdown-footer">---</span>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="fas fa-user"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <a href="javascript:;" class="dropdown-item" data-toggle="modal" data-target="#modalChangePassword">
                            <i class="fas fa-lock mr-2"></i> Ubah Kata Sandi
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="{{ route('auth.storeLogout') }}" class="dropdown-item">
                            <i class="fas fa-sign-out-alt mr-2"></i> Keluar
                        </a>
                    </div>
                </li>
            </ul>
        </nav>

        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <a href="#" class="brand-link text-center text-uppercase">
                <span class="brand-text font-weight-bold">Lapor</span>
            </a>

            <div class="sidebar">
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img
                            src="{{ showPhotoProfile(Auth::user()->photo) }}"
                            class="img-circle elevation-2"
                            alt="User Image" style="width: 34px; height: 34px;">
                    </div>
                    <div class="info">
                        <a href="javascript:;" class="d-block">{{ Auth::user()->name }}</a>
                    </div>
                </div>

                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">

                        {{-- Sidemenu --}}
                        @include('components/sideMenu')

                    </ul>
                </nav>

            </div>
        </aside>

        <div class="content-wrapper">
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">@yield('title-page')</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Home</a></li>
                                <li class="breadcrumb-item active">@yield('title-page')</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content">
                <div class="container-fluid">

                    {{-- Success Message --}}
                    @if (Session::get('success') != null)
                    <div class="alert alert-info alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5><i class="icon fas fa-check"></i> Berhasil!</h5>
                        {{ Session::get('success') }}
                    </div>
                    @endif

                    {{-- Error Message --}}
                    @if (Session::get('error') != null)
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5><i class="icon fas fa-ban"></i> Terjadi Kesalahan!</h5>
                        {{ Session::get('error') }}
                    </div>
                    @endif

                    {{-- Error Message --}}
                    @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5><i class="icon fas fa-ban"></i> Terjadi Kesalahan!</h5>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    @yield('content')

                </div>
            </div>
        </div>

        <footer class="main-footer">
            <strong>Copyright &copy; 2021 <a href="#">UIN Psikologi.</a></strong> All rights reserved.
        </footer>
    </div>
@endsection

@push('custom-script')
<script>
$(function() {
    let valUrl = @json(route('notification.getData'));
    $.ajax({
        url: valUrl,
        method: 'POST'
    })
    .done(function(res) {
        let elCount = $('.notif-count');
        let elCountBar = $('.notif-bar-count');
        let elContent = $('.notif-content');

        let countNotif = res.countNotif;
        let data = res.data;

        $.each(data, function(index, value) {
            let localTime = moment.utc(value.created_at).toDate(); 
            let relativeTime = moment(localTime).fromNow();

            elContent.append(`
                <div class="dropdown-divider"></div>
                <a href="` + @json(route('notification.redirectData')) + `?id=` + value.id + `&url=` + (value.url_direct != null ? value.url_direct : '') + `" class="dropdown-item">
                    <h6 class="text-muted text-sm">
                        <i class="fas fa-clock mr-1"></i>
                        ` + relativeTime + `
                    </h6>
                    <b>` + value.title + `</b>
                    <p>` + value.message + `</p>
                </a>
            `);
        });

        if (countNotif == 0)
            $('.notif-count').hide();

        elCount.html(countNotif);
        elCountBar.html(countNotif);
    });

    $('#modalSendMessage').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget);
      var user = button.data('user');
      var name = button.data('name');
      
      var modal = $(this);
      if (name != null) modal.find('#sendTo').text('ke ' + name);
      modal.find('#user').val(user);
    })
})
</script>
@endpush