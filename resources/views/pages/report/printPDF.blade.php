<html>
<head>
  <title>Laporan Persetujuan Dosen</title>
  <style>
    .text-center {
      text-align: center;
    }

    .text-uppercase {
      text-transform: uppercase;
    }

    .m-0 {
      margin: 0px;
    }

    .p-0 {
      padding: 0px;
    }
  </style>
</head>
<body>
  <h3 class="text-center text-uppercase m-0 p-0">Laporan Persetujuan Dosen {{ Auth::user()->name }}</h3>
  <h3 class="text-center text-uppercase m-0 p-0">
    {{ $month != null ? "Bulan " . getMonth($month) : '' }}
    {{ $year != null ? "Tahun $year" : '' }}
  </h3>
  <br>
  <table border="1" cellpadding="5" cellspacing="0" style="width: 100%">
    <thead>
      <tr>
        <th>No</th>
        <th>NIM</th>
        <th>Nama Mahasiswa</th>
        <th>Semester</th>
        <th>Kegiatan</th>
        <th>Skor</th>
        <th>Status</th>
        <th>Tanggal Disetujui</th>
      </tr>
    </thead>
    <tbody>
      @if (count($data) > 0)
        @php $indexCount = 0 @endphp
        @foreach ($data as $row)
          <tr>
            <td class="text-center">{{ $indexCount + 1 }}</td>
            <td class="text-center">{{ $row->profileUser->user->no_id }}</td>
            <td>{{ $row->profileUser->user->name }}</td>
            <td class="text-center">{{ $row->profileUser->semester->name }}</td>
            <td class="text-center">{{ $row->typeActivity->activity->name }}</td>
            <td class="text-center">{{ $row->total_score }} point</td>
            <td class="text-center">{{ 
              $row->status != 'WAITING' ? 
                ($row->status == 'VERIFY' ? 'Disetujui' : 'Ditolak') 
                : 'Menunggu Konfirmasi'
            }}</td>
            <td class="text-center">{{ $row->approved_at->isoFormat('dddd, D MMMM Y') }}</td>
          </tr>

          @php $indexCount++ @endphp
        @endforeach
      @else
        <tr class="text-center">
          <td colspan="8">Tidak ada data</td>
        </tr>
      @endif
    </tbody>
  </table>
</body>
</html>