<form action="{{ route('report.indexDosen') }}" method="GET">
  <div class="row mb-3">
    <div class="col-6 col-md-2 mb-2">
      <label>Bulan</label>
      <select class="form-control form-control-sm select2" name="month" id="month">
        <option value="">-- Semua Bulan --</option>
        <option value="01" {{ Request::get('month') == '01' ? 'selected' : '' }}>Januari</option>
        <option value="02" {{ Request::get('month') == '02' ? 'selected' : '' }}>Februari</option>
        <option value="03" {{ Request::get('month') == '03' ? 'selected' : '' }}>Maret</option>
        <option value="04" {{ Request::get('month') == '04' ? 'selected' : '' }}>April</option>
        <option value="05" {{ Request::get('month') == '05' ? 'selected' : '' }}>Mei</option>
        <option value="06" {{ Request::get('month') == '06' ? 'selected' : '' }}>Juni</option>
        <option value="07" {{ Request::get('month') == '07' ? 'selected' : '' }}>Juli</option>
        <option value="08" {{ Request::get('month') == '08' ? 'selected' : '' }}>Agustus</option>
        <option value="09" {{ Request::get('month') == '09' ? 'selected' : '' }}>September</option>
        <option value="10" {{ Request::get('month') == '10' ? 'selected' : '' }}>Oktober</option>
        <option value="11" {{ Request::get('month') == '11' ? 'selected' : '' }}>November</option>
        <option value="12" {{ Request::get('month') == '12' ? 'selected' : '' }}>Desember</option>
      </select>
    </div>
    <div class="col-6 col-md-2 mb-2">
      <label>Tahun</label>
      <select class="form-control form-control-sm select2" name="year" id="year">
        @for ($i = 2021; $i <= 2030; $i++)
          <option value="{{ $i }}" {{ 
              Request::get('year') != null ? 
                (Request::get('year') == $i ? 'selected' : '') :
                (date('Y') == $i ? 'selected' : '')
            }}>{{ $i }}</option>
        @endfor
      </select>
    </div>
    <div class="col-6 col-md-2 align-self-end mb-2">
      <button class="btn btn-primary btn-sm">Cari</button>
    </div>
    <div class="col-6 col-md-6 align-self-end mb-2 text-right">
      <a class="btn btn-danger btn-sm" href="{{ route('report.printPDF', ['year' => Request::get('year'), 'month' => Request::get('month')]) }}">Print PDF</a>
    </div>
  </div>
</form>