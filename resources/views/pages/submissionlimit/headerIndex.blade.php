<a class="btn btn-primary btn-sm" href="javascript:;" data-toggle="modal" data-target="#modalSubmissionLimit">Buat Pengajuan</a>
<br><br>

<!-- Modal Submission Limit -->
<div class="modal fade" id="modalSubmissionLimit" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <form method="POST" action="{{ route('submissionLimit.store') }}" autocomplete="off">
      @csrf
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Pengajuan Batas Persetujuan Dosen per-hari</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label for="limitApproval">Batas Persetujuan</label>
          <input type="numeric" class="form-control" id="limitApproval" name="limitApproval" placeholder="0" required>
        </div>
        <div class="form-group">
          <label for="usedDate">Digunakan pada Tanggal</label>
          <input type="date" class="form-control" id="usedDate" name="usedDate" placeholder="Digunakan pada Tanggal" min="{{ now()->toDateString('Y-m-d') }}">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
    </form>
    </div>
  </div>
</div>