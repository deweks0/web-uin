@extends('layouts.panel')

@section('title-page') {{ $titlePage }} @endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class="card-header">
        <h3 class="card-title text-white">{{ $titlePage }}</h3>
      </div>

      <div class="card-body">
        <x-table-list-item :urlData="$urlData" :headerColumns="$headerColumns" :dataColumns="$dataColumns" />
      </div>
    </div>
  </div>
</div>
@endsection