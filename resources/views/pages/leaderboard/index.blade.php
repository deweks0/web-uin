@extends('layouts.panel')

@section('title-page') {{ $titlePage }} @endsection
@section('content')

@push('custom-style')
  <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
  <style>
    .list-leaderboard .card .card-body {
      padding: 5px 0px;
      padding-bottom: 15px;
    }

    .list-leaderboard .card .card-body .card-title {
      font-size: 30px;
      margin: 0px !important;
    }
  </style>
@endpush

<div class="row">
  <div class="col-md-12">
    <div class="card card-info">
      <div class="card-header">
        <h3 class="card-title text-white">{{ $titlePage }}</h3>
      </div>

      <div class="card-body">

        <form action="{{ route('leaderboard.postIndex') }}" method="POST">
          @csrf
          <div class="row">
            <div class="col-sm-6 col-md-2 text-left">
              <label class="d-block">
                Tingkat
                <select name="degree" class="form-control form-control-sm opt-degree">
                  @if (\Auth::user()->role->code == 'MHS')
                    <option value="ONE_DEGREE" {{ $semester == 'ONE_DEGREE' ? 'selected' : '' }}>Satu Tingkat</option>
                  @endif
                  <option value="ALL_DEGREE" {{ $semester == 'ALL_DEGREE' ? 'selected' : '' }}>Semua Tingkat</option>
                </select>
              </label>
            </div>
            <div class="col-sm-6 col-md-2 text-left">
              <label class="d-block">
                Kategori Kegiatan
                <select name="typeActivity" class="form-control form-control-sm opt-type-activity">
                  <option value="">- Semua -</option>
                  @foreach ($optActivity as $index => $row)
                    <option value="{{ $index }}" {{ $index == $typeActivity ? 'selected' : '' }}>{{ $row }}</option>
                  @endforeach
                </select>
              </label>
            </div>
            <div class="col-sm-1 align-self-end">
              <button class="btn btn-primary btn-sm mb-2">Cari</button>
            </div>
          </div>
        </form>

        <div class="row mt-4 list-leaderboard">

          @if (count($data) > 0)
            @php $countRow = 0; @endphp
            @foreach ($data as $row)
            <div class="col-12">
              <div class="card bg-{{ $colorBg[($countRow)] }}">
                <div class="card-body">
                  <div class="row">
                    <div class="col-2 col-md-1 align-self-center text-center">
                      <h2 class="text-center">{{ $countRow + 1 }}</h1>
                    </div>

                    <div class="col-10 col-md-11">
                      <div class="row">
                        <div class="col-12 col-md-8 mb-3">
                          <p class="card-title mb-2">{{ ucwords($row->user->name) }}</p>

                          {{-- Star --}}
                          <p class="card-text">

                            @php
                              $profileScore = $row->profile_score;
                              $countStar = 0;

                              if ($profileScore < 50)
                                $countStar = floor($profileScore / 10);
                              else
                                $countStar = floor($profileScore / 50);
                            @endphp

                            @for ($i = 0; $i < $countStar; $i++)
                              <img src="{{ asset('img/ic-star-' . ($profileScore < 50 ? 'silver' : 'gold') . '.svg') }}" width="30px" height="30px"/>
                            @endfor

                          </p>
                        </div>
                        <div class="col-12 col-md-4 align-self-center pr-4">
                          <h1>{{ $row->profile_score }}<small>poin</small></h1>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            @php $countRow++; @endphp
            @endforeach
          @else
            <div class="col-12">
              <div class="alert alert-light" role="alert">
                Tidak ada data
              </div>
            </div>
          @endif

        </div>

      </div>
    </div>
  </div>
</div>
@endsection