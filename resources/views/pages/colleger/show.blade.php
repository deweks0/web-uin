@extends('layouts.panel')

@push('custom-style')
<style>
.info-box-icon {
  padding: 20px 0px;
}

.info-box-content {
  padding: 0px !important;
  padding-top: 5px !important;
  display: inline-block;
}

@media only screen and (min-width: 600px) {
  .info-box-content {
    padding: 10px !important;
    display: inline-block;
  }
}
</style>
@endpush

@section('title-page') {{ $titlePage }} @endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">{{ $titlePage }}</h3>
        <div class="card-tools">
            <a href="{{ route('colleger.printPDF', $user_id) }}" class="btn btn-danger btn-sm">Cetak PDF</a>
        </div>
      </div>
      <div class="card-body">
        <div class="row">

          {{-- Card Profile --}}
          <div class="col-12 col-md-3">
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                    src="{{ showPhotoProfile($data->photo) }}"
                    style="width: 100px; height: 100px;"
                    alt="Photo Profile">
                </div>

                <h3 class="profile-username text-center">{{ $data->name }}</h3>
                <p class="text-muted text-center">{{ $data->role->name }}</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <span class="float-left">
                      <i class="fas fa-phone mr-2"></i>
                      {{ $data->phone != null ? $data->phone : '-' }}
                    </span>
                  </li>
                  <li class="list-group-item">
                    <span class="float-left">
                      <i class="fas fa-address-book mr-2"></i>
                      {{ $data->address != null ? $data->address : '-' }}
                    </span>
                  </li>
                </ul>
              </div>
            </div>
          </div>

          {{-- Content Profile --}}
          <div class="col-12 col-md-9">

            <div class="container">

              {{-- Info Boxes --}}
              <div class="row">

                <div class="col-4">
                  <div class="info-box">
                    <div class="row no-gutters w-100">
                      <div class="col-md-4">
                        <span class="info-box-icon bg-primary elevation-1 h-100 w-100">
                          <i class="fas fa-arrow-up"></i>
                        </span>
                      </div>
                      <div class="col-md-8">
                        <div class="info-box-content">
                          <span class="info-box-text">Total Point</span>
                          <span class="info-box-number">{{ $data->profile->profile_score }} Point</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
          
                <div class="col-4">
                  <div class="info-box">
                    <div class="row no-gutters w-100">
                      <div class="col-md-4">
                        <span class="info-box-icon bg-success elevation-1 h-100 w-100">
                          <i class="fas fa-users"></i>
                        </span>
                      </div>
                      <div class="col-md-8">
                        <div class="info-box-content">
                          <span class="info-box-text">Organisasi</span>
                          <span class="info-box-number">{{ $collectPoint['organisasi'] }} Point</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
          
                <div class="col-4">
                  <div class="info-box">
                    <div class="row no-gutters w-100">
                      <div class="col-md-4">
                        <span class="info-box-icon bg-warning elevation-1 h-100 w-100">
                          <i class="fas fa-medal fa-icon-white"></i>
                        </span>
                      </div>
                      <div class="col-md-8">
                        <div class="info-box-content">
                          <span class="info-box-text">Minat & Bakat</span>
                          <span class="info-box-number">{{ $collectPoint['minat_bakat'] }} Point</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
          
                <div class="col-4">
                  <div class="info-box">
                    <div class="row no-gutters w-100">
                      <div class="col-md-4">
                        <span class="info-box-icon bg-danger elevation-1 h-100 w-100">
                          <i class="fas fa-chart-line"></i>
                        </span>
                      </div>
                      <div class="col-md-8">
                        <div class="info-box-content">
                          <span class="info-box-text">Pengabdian</span>
                          <span class="info-box-number">{{ $collectPoint['pengabdian'] }} Point</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
          
                <div class="col-4">
                  <div class="info-box">
                    <div class="row no-gutters w-100">
                      <div class="col-md-4">
                        <span class="info-box-icon bg-info elevation-1 h-100 w-100">
                          <i class="fas fa-university"></i>
                        </span>
                      </div>
                      <div class="col-md-8">
                        <div class="info-box-content">
                          <span class="info-box-text">Kewirausahaan</span>
                          <span class="info-box-number">{{ $collectPoint['kewirausahaan'] }} Point</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
          
                <div class="col-4">
                  <div class="info-box">
                    <div class="row no-gutters w-100">
                      <div class="col-md-4">
                        <span class="info-box-icon bg-info elevation-1 h-100 w-100">
                          <i class="fas fa-book"></i>
                        </span>
                      </div>
                      <div class="col-md-8">
                        <div class="info-box-content">
                          <span class="info-box-text">Keilmuan</span>
                          <span class="info-box-number">{{ $collectPoint['keilmuan'] }} Point</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
          
              </div>

              {{-- User Activity --}}
              <div class="row">
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">Prestasi Mahasiswa</h3>
                    </div>

                    <div class="card-body">
                      @if (count($data->activity) > 0)
                      <ul style="padding: 0px; margin: 0px; margin-left: 20px;">
                        @foreach ($data->activity as $row)
                          <li><a href="{{ url('uploads/file_document/' . $row->file_document) }}">{{ $row->name != null ? $row->name : $row->file_document }}</a></li>
                        @endforeach
                      </ul>
                      @else
                        Tidak ada data prestasi
                      @endif
                    </div>
                  </div>
                </div>
              </div>

              {{-- Portofolio --}}
              <div class="row">
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">Portofolio</h3>
                    </div>

                    <div class="card-body">
                      <x-table-list-item :urlData="$urlData" :headerColumns="$headerColumns" :dataColumns="$dataColumns" />
                    </div>
                  </div>
                </div>
              </div>

            </div>

          </div>

        </div>
      </div>
    </div>
  </div>
</div>
@endsection