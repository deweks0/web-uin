@extends('layouts.master')

@push('custom-style')
<style>
    :root{
        --primary-color: #1e3799;
        --secondary-color: #3498db;
    }
    
    .primary-bg{
        background-color: var(--primary-color);
    }

    .secondary-bg{
        background-color: var(--secondary-color);
    }

    .img-info {
        max-width: 28rem;
        max-height: 28rem;
        min-width: 10rem;
        min-height: 10rem;
    }

    .img-info-small {
        max-width: 100%;
        height: auto;
    }

    .logo{
        width:3rem;
    }

   .navbar {
        transition: top 0.5s ease;
    }

    .navbar-hide {
        top: -100px;
    }

    .carousel img{
        height: 100vh;
    }
</style>
@endpush

@section('body')
<!-- navbar -->
<nav class="navbar navbar-expand-lg fixed-top  navbar-dark primary-bg">
  <div class="d-flex align-items-center navbar-brand">
    <img src="https://i0.wp.com/ilmuhukum.uinsgd.ac.id/wp-content/uploads/2018/01/jyrqKmCZ-1.png?w=1830&ssl=1" alt="" class="logo">
    <h5 class="navbar-text ml-2">KEMAHASISWAAN <br/> PSIKOLOGI</h5>
</div>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto text-white">
        <li class="nav-item active">
            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Organisasi Mahasiswa</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Alumni</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Lapor</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Kerjasama</a>
        </li>
    </ul>
  </div>
</nav>
<!-- end of navbar -->

<!-- carousel -->
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="https://fpscs.uii.ac.id/psychology/wp-content/uploads/sites/2/2019/10/student-uii.jpg"
                class="w-100" alt="">
        </div>
        <div class="carousel-item">
            <img src="https://www.uii.ac.id/wp-content/uploads/2017/02/IMG_99931.jpg" class="w-100" alt="...">
        </div>
        <div class="carousel-item">
            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSIopttKZ_otxvtn6GGkBCJGPCDNvXv1WMcPA&usqp=CAU"
                class="w-100" alt="...">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<!-- end of carousel -->

<!-- content -->
<div class="text-center py-2 secondary-bg">
    <h5 class=" text-white">Berita Fakultas Psikologi</h5>
</div>

<div class="container my-3" >
    <div class="row">
        <div class="col-md-5 my-2 my-md-0 d-flex w-100 h-100 justify-content-center align-items-center">
            <!-- <div class=""> -->
                <img src="https://doktor.psikologi.unair.ac.id/wp-content/uploads/2018/02/MG_8128-res.jpg" class="img-info" />
            <!-- </div> -->
        </div>
        <div class="col-md-7 my-2 my-md-0 d-flex flex-wrap align-content-between">
            <div class="row mb-3 mb-md-0">
                <div class="col-4">
                    <img src="https://doktor.psikologi.unair.ac.id/wp-content/uploads/2018/02/MG_8128-res.jpg"
                        class="img-info-small " />
                </div>
                <div class="col-4">
                    <img src="https://doktor.psikologi.unair.ac.id/wp-content/uploads/2018/02/MG_8128-res.jpg"
                        class="img-info-small " />
                </div>
                <div class="col-4">
                    <img src="https://doktor.psikologi.unair.ac.id/wp-content/uploads/2018/02/MG_8128-res.jpg"
                        class="img-info-small " />
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <img src="https://doktor.psikologi.unair.ac.id/wp-content/uploads/2018/02/MG_8128-res.jpg"
                        class="img-info-small " />
                </div>
                <div class="col-4">
                    <img src="https://doktor.psikologi.unair.ac.id/wp-content/uploads/2018/02/MG_8128-res.jpg"
                        class="img-info-small " />
                </div>
                <div class="col-4">
                    <img src="https://doktor.psikologi.unair.ac.id/wp-content/uploads/2018/02/MG_8128-res.jpg"
                        class="img-info-small " />
                </div>
            </div>
        </div>
    </div>
</div>

<div class="text-center py-2 secondary-bg">
    <h5 class=" text-white">Event Fakultas Psikologi</h5>
</div>

<div class="container my-3">
    <div class="row">
        <div class="col-md-5 d-flex justify-content-center align-items-center">
            <img src="https://doktor.psikologi.unair.ac.id/wp-content/uploads/2018/02/MG_8128-res.jpg" alt="" class="img-info w-100">  
        </div>
        <div class="col-md-7">
            <div class="d-flex  flex-column h-100">
                <h2>List Event</h2>
                <div class="rounded secondary-bg p-2 text-white">
                    @foreach ($events as $event)
                    <div class="d-flex justify-content-between border-bottom align-items-end border-white">
                        <h5>{{$event->name}}</h5>
                        <p>{{$event->created_at->format('d/m/Y')}}</p>
                    </div>
                    @endforeach
                    <div class="d-flex justify-content-center align-items-end mt-5 mb-3">
                        <a class="bg-dark px-4 py-2 rounded-pill" href="#">
                            Event Terdahulu
                        </a>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</div>

<div class="text-center py-2 secondary-bg">
    <h5 class=" text-white">Mahasiswa Berprestasi</h5>
</div>

<div class="container mt-3">
    <div class="row">
        @foreach($users as $user)
        <div class="col-md-4 d-flex justify-content-center align-items-center">
            <div class="card rounded-0" style="max-width: 18rem;">
                <img src="{{showPhotoProfile($user->photo) }}"
                    class="card-img-top" alt="...">
                <div class="card-body text-white text-center secondary-bg">
                    <h5 class="text-bold">{{$user->name}}</h5>
                    <p class="text-thin">Mahasiswa {{$user->generation}}</p>
                    <h5 class="text-bold">Score</h5>
                    <p class="text-thin">{{$user->profile->profile_score}}</p>
                    <h5 class="text-bold">IPK</h5>
                    <p class="text-thin">3.9</p>
                    <a class="bg-dark px-5 py-2 rounded-pill" href="#">
                        Detail
                    </a>
                </div>
            </div>
        </div>
        @endforeach
        <!-- <div class="col-md-4 d-flex justify-content-center align-items-center">
            <div class="card rounded-0" style="width: 18rem;">
                <img src="https://doktor.psikologi.unair.ac.id/wp-content/uploads/2018/02/MG_8128-res.jpg"
                    class="card-img-top" alt="...">
                <div class="card-body text-white text-center secondary-bg">
                    <h5 class="text-bold">Lutfiyan Pangaribun</h5>
                    <p class="text-thin">Angkatan 2019</p>
                    <h5 class="text-bold">Score</h5>
                    <p class="text-thin">203</p>
                    <h5 class="text-bold">IPK</h5>
                    <p class="text-thin">3.9</p>
                    <a class="bg-dark px-5 py-2 rounded-pill" href="#">
                        Detail
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-4 d-flex justify-content-center align-items-center">
            <div class="card rounded-0" style="width: 18rem;">
                <img src="https://doktor.psikologi.unair.ac.id/wp-content/uploads/2018/02/MG_8128-res.jpg"
                    class="card-img-top" alt="...">
                <div class="card-body text-white text-center secondary-bg">
                    <h5 class="text-bold">Heru Sulaiman</h5>
                    <p class="text-thin">Angkatan 2020</p>
                    <h5 class="text-bold">Score</h5>
                    <p class="text-thin">203</p>
                    <h5 class="text-bold">IPK</h5>
                    <p class="text-thin">3.9</p>
                    <a class="bg-dark px-5 py-2 rounded-pill" href="#">
                        Detail
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-4 d-flex justify-content-center align-items-center">
            <div class="card rounded-0" style="width: 18rem;">
                <img src="https://doktor.psikologi.unair.ac.id/wp-content/uploads/2018/02/MG_8128-res.jpg"
                    class="card-img-top" alt="...">
                <div class="card-body text-white text-center secondary-bg">
                    <h5 class="text-bold">Algeansyah Syahfitri</h5>
                    <p class="text-thin">Angkatan 2019</p>
                    <h5 class="text-bold">Score</h5>
                    <p class="text-thin">203</p>
                    <h5 class="text-bold">IPK</h5>
                    <p class="text-thin">3.9</p>
                    <a class="bg-dark px-5 py-2 rounded-pill" href="#">
                        Detail
                    </a>
                </div>
            </div>
        </div> -->
    </div>
</div>
<!-- end of content -->

<!-- footer -->
<footer class="py-3 secondary-bg">
    <div class="border-bottom border-white">
        <div class="container d-flex justify-content-between text-white align-items-center ">
            <div>
                <h4>UIN Sunan Gunung Djati</h4>
            </div>
            <div>
                <a href="" class="text-light">  
                    <i class="bi bi-instagram mx-1" style="font-size: 1.5rem; mx-1" style="font-size: 1.5rem;"></i>
                </a>
                <a href="" class="text-light">  
                    <i class="bi bi-youtube mx-1" style="font-size: 1.5rem;"></i>
                </a>
                <a href="" class="text-light">
                    <i class="bi bi-linkedin mx-1" style="font-size: 1.5rem;"></i>
                </a>
                <a href="" class="text-light">  
                    <i class="bi bi-facebook mx-1" style="font-size: 1.5rem;"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="container d-flex flex-column mt-3">
        <a href="" class="text-light">Aplikasi Lapor</a>
        <a href="" class="text-light">Aplikasi Organisasi Mahasiswa</a>
        <a href="" class="text-light">Alumni</a>
        <a href="" class="text-light">Kontak</a>
    </div>
</footer>
<!-- end of footer -->
@endsection

@push('custom-script')
<script src="https://unpkg.com/scroll-detector@0.6.1/dist/scroll-detector.min.js"></script>
<script>
    $(document).ready(function(){
        const navbarCollapse = $('.navbar-collapse')

        scrollDetector.on( 'scroll:up', () => {
            if(!navbarCollapse.hasClass('show')) $('.navbar').removeClass("navbar-hide");
        });

        scrollDetector.on( 'scroll:down', () => {
            if(!navbarCollapse.hasClass('show')) $('.navbar').addClass("navbar-hide");
        });
        // $(window).scroll(function(e) {
        //     if (scroll >= 150 && !navbarCollapse.hasClass('show')) {
        //         $('.navbar').addClass("navbar-hide");
        //     } else {
        //         $('.navbar').removeClass("navbar-hide");
        //     }    
        // });
    });
</script>
@endpush