@extends('layouts.master')

<div class="container h-100">
  <div class="row align-items-center h-100">
    <div class="col-8 mx-auto text-center">
      <h1>Oopps..</h1>
      <h3 class="mt-3">Website ini sedang dalam masa maintenance. Mohon coba beberapa saat lagi...</h3>
    </div>
  </div>
</div>