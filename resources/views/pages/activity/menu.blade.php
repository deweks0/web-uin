@extends('layouts.panel')

@push('custom-style')
<style>
  .bg-warning,
  .small-box.bg-warning .small-box-footer {
    color: #fff !important;
  }

  .small-box {
    position: relative;
    height: 150px;
  }

  .small-box .inner {
    width: 60%;
    height: 100%;
  }

  .small-box .small-box-footer {
    bottom: 0px;
    left: 0px;
    right: 0px;
    position: absolute;
  }

  .small-box .icon-content {
    font-size: 70px;
    color: rgba(0,0,0,.15);
  }
</style>
@endpush

@section('title-page') {{ $titlePage }} @endsection
@section('content')
<!-- Info boxes -->
<div class="row">

  <div class="col-6 col-lg-3">
    <div class="small-box bg-info">
      <div class="container">
        <div class="row">
          <div class="col-8 align-self-center text-center">
            <h2 class="title mt-3">Penalaran dan Keilmuan</h2>
          </div>
          <div class="col-3 text-center">
            <i class="icon-content mt-3 fas fa-book"></i>
          </div>
        </div>
      </div>
      <a href="{{ route('activity.create', 'keilmuan') }}" class="small-box-footer">
        Masukkan Data <i class="fas fa-arrow-circle-right ml-2"></i>
      </a>
    </div>
  </div>

  <div class="col-6 col-lg-3">
    <div class="small-box bg-success">
      <div class="container">
        <div class="row">
          <div class="col-8 align-self-center text-center">
            <h2 class="title mt-3">Organisasi dan Kepemimpinan</h2>
          </div>
          <div class="col-3 text-center">
            <i class="icon-content mt-3 fas fa-users"></i>
          </div>
        </div>
      </div>
      <a href="{{ route('activity.create', 'kepemimpinan') }}" class="small-box-footer">
        Masukkan Data <i class="fas fa-arrow-circle-right ml-2"></i>
      </a>
    </div>
  </div>

  <div class="col-6 col-lg-3">
    <div class="small-box bg-warning">
      <div class="container">
        <div class="row">
          <div class="col-8 align-self-center text-center">
            <h2 class="title mt-3">Minat dan Bakat</h2>
          </div>
          <div class="col-3 text-center">
            <i class="icon-content mt-3 fas fa-medal"></i>
          </div>
        </div>
      </div>
      <a href="{{ route('activity.create', 'minat-bakat') }}" class="small-box-footer">
        Masukkan Data <i class="fas fa-arrow-circle-right ml-2"></i>
      </a>
    </div>
  </div>

  <div class="col-6 col-lg-3">
    <div class="small-box bg-danger">
      <div class="container">
        <div class="row">
          <div class="col-8 align-self-center text-center">
            <h2 class="title mt-3">Pengabdian dalam Masyarakat</h2>
          </div>
          <div class="col-3 text-center">
            <i class="icon-content mt-3 fas fa-chart-line"></i>
          </div>
        </div>
      </div>
      <a href="{{ route('activity.create', 'pengabdian') }}" class="small-box-footer">
        Masukkan Data <i class="fas fa-arrow-circle-right ml-2"></i>
      </a>
    </div>
  </div>

  <div class="col-6 col-lg-3">
    <div class="small-box bg-info">
      <div class="container">
        <div class="row">
          <div class="col-8 align-self-center text-center">
            <h2 class="title mt-3">Kewirausahaan</h2>
          </div>
          <div class="col-3 text-center">
            <i class="icon-content mt-3 fas fa-university"></i>
          </div>
        </div>
      </div>
      <a href="{{ route('activity.create', 'kewirausahaan') }}" class="small-box-footer">
        Masukkan Data <i class="fas fa-arrow-circle-right ml-2"></i>
      </a>
    </div>
  </div>

</div>
@endsection