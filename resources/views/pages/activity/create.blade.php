@extends('layouts.panel')

@section('title-page') {{ $titlePage }} @endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-{{ $panelColor }}">
      <div class="card-header">
        <h3 class="card-title text-white">{{ $titlePage }}</h3>
      </div>
      
      <div class="card-body">
        <form action="{{ route('activity.store') }}" method="POST" enctype="multipart/form-data">
          @csrf

          <x-form-data :pathView="'formData.activity'" :options="$options" :data="isset($data) ? $data : []" />

          <button type="submit" class="btn btn-info mt-5">Simpan</button>

        </form>
      </div>
    </div>
  </div>
</div>
@endsection