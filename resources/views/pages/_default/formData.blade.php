@extends('layouts.panel')

@section('title-page') {{ $titlePage }} @endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">{{ $titlePage }}</h3>
      </div>

      <div class="card-body">
        <form action="{{ $formUrl }}" method="POST" enctype="multipart/form-data">
          @csrf
          @method(isset($formMethod) ? $formMethod : 'POST')

          <x-form-data :pathView="$urlComponent" :options="isset($options) ? $options : []" :data="isset($data) ? $data : []" />

          <button type="submit" class="btn btn-primary btn-sm mt-5">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection