<div class="row">
  <div class="col-12">

    <div class="row">
      <div class="col-sm-3">
        <div class="text-center">
          <img class="img-responsive img-thumbnail img-circle mb-3 img-preview-profile" 
            src="{{ showPhotoProfile($data->photo) }}"
            alt="No Image" style="width: 200px; height: 200px;">
        </div>
        <div class="input-group mb-3">
          <input type="file" name="photo" class="form-control input-upload-photo">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-id-card"></span>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-3">
        <label>NIM/NIP</label>
        <div class="input-group mb-3">
          <input type="text" name="no_id" class="form-control" placeholder="NIP/NIM" autocomplete="off" value="{{ isset($data->no_id) ? $data->no_id : null }}" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-id-card"></span>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-3">
        <label>Nama Lengkap</label>
        <div class="input-group mb-3">
          <input type="text" name="name" class="form-control" placeholder="Nama Lengkap" value="{{ isset($data->name) ? $data->name : null }}" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-3">
        <label>Email</label>
        <div class="input-group mb-3">

          @if (empty($data->email))
          <input type="email" name="email" class="form-control" placeholder="Email" value="{{ isset($data->email) ? $data->email : null }}" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          @else
          <input class="form-control" placeholder="Email" value="{{ isset($data->email) ? $data->email : null }}" disabled>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>

          <input type="hidden" name="email" value="{{ isset($data->email) ? $data->email : null }}">
          @endif

        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-3">
        <label>No. HP / Telepon</label>
        <div class="input-group mb-3">
          <input type="number" name="phone" class="form-control" placeholder="No. HP/Telepon" value="{{ isset($data->phone) ? $data->phone : null }}">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-phone"></span>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-3">
        <label>Alamat</label>
        <div class="input-group mb-3">
          <textarea name="address" class="form-control" rows="3" placeholder="Alamat">{{ isset($data->address) ? $data->address : null }}</textarea>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-address-book"></span>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row content-select-semester">
      <div class="col-sm-3">
      <label>Tahun Angkatan</label>
      <input name="generation" type="text" class="form-control" placeholder="Tahun Angkatan" value="{{ isset($data['generation']) ? $data['generation'] : null }}">
      </div>
    </div>

    <div class="row content-select-semester">
      <div class="col-sm-3">
      <label>Semester</label>
        <div class="form-group">
          <select name="semester" class="form-control input-block select2">
            <option value="" disabled>-- Pilih --</option>

            @php
              // Check if semester was selected before
              $selectedIndex = true;
              $countIndex = 0;
            @endphp
            @foreach ($options['optSemester'] as $index => $row)
              @php
                $statSelected = isset($data->profile->semester_id) && $data->profile->semester_id == $index;
                if ($statSelected)
                  $selectedIndex = false
              @endphp

              <option value="{{ $index }}"
                {{ $statSelected ? 'selected' : ($selectedIndex ? 'disabled' : '') }}>
                {{ $row }}
              </option>

              @php $countIndex++ @endphp
            @endforeach

          </select>
        </div>
      </div>
    </div>

  </div>
</div>

@push('custom-script')
<script>
function readURL(input, index, className) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $(className).eq(index).attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$(function() {
  $('.input-upload-photo').change(function() {
    readURL(this, 0, '.img-preview-profile');
  });
});
</script>
@endpush