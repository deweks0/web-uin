<div class="row">
    <div class="col-sm-2">
        <div class="form-group">
            <label>Batas Persetujuan Dosen per-hari</label>
            <input name="limitApproval" type="number" class="form-control" placeholder="Poin" value="{{ isset($data['limit_approval']) ? $data['limit_approval'] : null }}">
        </div>
    </div>
</div>