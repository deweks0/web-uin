<div class="row">
  <div class="col-12">

    <div class="row">
      <div class="col-sm-3">
        <label>NIM/NIP</label>
        <div class="input-group mb-3">
          <input type="text" name="no_id" class="form-control" placeholder="NIP/NIM" autocomplete="off" value="{{ isset($data->no_id) ? $data->no_id : null }}" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-id-card"></span>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-3">
        <label>Nama Lengkap</label>
        <div class="input-group mb-3">
          <input type="text" name="name" class="form-control" placeholder="Nama Lengkap" value="{{ isset($data->name) ? $data->name : null }}" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-3">
        <label>Email</label>
        <div class="input-group mb-3">

          @if (empty($data->email))
          <input type="email" name="email" class="form-control" placeholder="Email" value="{{ isset($data->email) ? $data->email : null }}" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          @else
          <input class="form-control" placeholder="Email" value="{{ isset($data->email) ? $data->email : null }}" disabled>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>

          <input type="hidden" name="email" value="{{ isset($data->email) ? $data->email : null }}">
          @endif

        </div>
      </div>
    </div>

    @if (empty($data))
      <div class="row">
        <div class="col-sm-12">
          <div class="form-group">
            <div class="form-check">
              <label class="form-check-label">
                <input class="form-check-input check-input-password" type="checkbox" name="passwordActive">
                Atur Kata Sandi
              </label>
              <br>
              <small>Apabila kata sandi tidak diatur, maka default password yang digunakan adalah "uin12345" tanpa tanda petik.</small>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-3">
          <label>Kata Sandi</label>
          <div class="input-group mb-3">
            <input type="password" name="password" class="form-control input-password" placeholder="Password" disabled>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <label class="mb-3" style="font-weight: normal">
            <input type="checkbox" class="show-input-password mr-1">
            Lihat Kata Sandi
          </label>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-3">
          <label>Konfirmasi Kata Sandi</label>
          <div class="input-group mb-3">
            <input type="password" name="password_confirmation" class="form-control input-password-confirm" placeholder="Konfirmasi Password" disabled>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
        </div>
      </div>
    @endif

    <div class="row">
      <div class="col-sm-3">
        <label>No. HP / Telepon</label>
        <div class="input-group mb-3">
          <input type="number" name="phone" class="form-control" placeholder="No. HP/Telepon" value="{{ isset($data->phone) ? $data->phone : null }}">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-phone"></span>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-3">
        <label>Alamat</label>
        <div class="input-group mb-3">
          <textarea name="address" class="form-control" rows="3" placeholder="Alamat">{{ isset($data->address) ? $data->address : null }}</textarea>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-address-book"></span>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-3">
        <div class="form-group">
          <label>Peran</label>
          <select name="role" class="form-control select-role select2" required>
            <option value="">-- Pilih --</option>
            @foreach ($options['optRole'] as $index => $row)
              <option value="{{ $index }}" 
                {{ isset($data->role->id) && $data->role->id == $index ? 'selected' : '' }}>
                {{ $row }}
              </option>
            @endforeach
          </select>
        </div>
      </div>
    </div>

    <div class="row content-select-semester d-none">
      <div class="col-sm-3">
      <label>Semester</label>
        <div class="form-group">
          <select name="semester" class="form-control input-block select2">
            <option value="">-- Pilih --</option>
            @foreach ($options['optSemester'] as $index => $row)
              <option value="{{ $index }}" 
                {{ isset($data->profile->semester_id) && $data->profile->semester_id == $index ? 'selected' : '' }}>
                {{ $row }}
              </option>
            @endforeach
          </select>
        </div>
      </div>
    </div>

  </div>
</div>

@push('custom-script')
<script>
// Method onChangeRole
function onChangeRole() {
  let valRole = $('.select-role').val();
  if (valRole == 3) {
    $('.content-select-semester').removeClass('d-none');
  } else {
    $('.content-select-semester').addClass('d-none');
  }
}

// Method onChangePassword
function onChangePassword() {
  let valChecked = $('.select-role').is(':checked');

  if (valChecked) {
    $('.input-password').removeAttr('disabled');
    $('.input-password-confirm').removeAttr('disabled');
  }

  if (! valChecked) {
    $('.input-password').attr('disabled', 'disabled');
    $('.input-password-confirm').attr('disabled', 'disabled');
  }
}

$(function() {
  onChangeRole();

  $('.check-input-password').on('change', function() {
    if ($(this).is(":checked")) {
      $('.input-password').removeAttr('disabled');
      $('.input-password-confirm').removeAttr('disabled');
    } else {
      $('.input-password').val('');
      $('.input-password-confirm').val('');
      
      $('.input-password').prop('disabled', true);
      $('.input-password-confirm').prop('disabled', true);
    }
  });

  $('.select-role').on('change', function() {
    onChangeRole();
  });
})
</script>
@endpush