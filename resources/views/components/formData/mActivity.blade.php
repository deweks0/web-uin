<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <label>Jenis Kegiatan</label>
            <select id="parentActivity" name="parentActivity" class="form-control select2" required>
                @foreach ($options['optParentAct'] as $index => $row)
                    <option value="{{ $index }}" 
                        {{ isset($data['code']) && $data['code'] == $index ? 'selected' : '' }}>
                        {{ $row }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
          <label>Nama</label>
          <input name="name" type="text" class="form-control" placeholder="Nama Kegiatan" value="{{ isset($data['name']) ? $data['name'] : null }}" required>
        </div>
    </div>
</div>