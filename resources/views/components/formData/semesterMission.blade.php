<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <label>Tingkat Semester</label>
            <select id="semester" name="semester" class="form-control select2">
                @foreach ($options['optSemester'] as $index => $row)
                    <option value="{{ $index }}" 
                        {{ isset($data['semester_id']) && $data['semester_id'] == $index ? 'selected' : '' }}>
                        {{ $row }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            <label>Jenis Bidang</label>
            <select id="type_activity" name="type_activity" class="form-control select2">
                @foreach ($options['optParentAct'] as $index => $row)
                    <option value="{{ $index }}" 
                        {{ isset($data['parent_activity_id']) && $data['parent_activity_id'] == $index ? 'selected' : '' }}>
                        {{ $row }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="col-sm-1">
        <div class="form-group">
            <label>Target Total Skor</label>
            <input name="target_total_score" type="number" class="form-control"  step="any" placeholder="Poin" value="{{ isset($data['target_total_score']) ? $data['target_total_score'] : null }}">
        </div>
    </div>
</div>