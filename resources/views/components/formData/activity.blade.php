<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Kategori Kegiatan</label>
            <select id="typeActivity" name="typeActivity" class="form-control select2">
                <option value="">-- Pilih --</option>
                @foreach ($options['optTypeAct'] as $index => $row)
                    <option value="{{ $index }}" 
                        {{ isset($data['activityId']) && $data['activityId'] == $index ? 'selected' : '' }}>
                        {{ $row }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Nama Kegiatan</label>
            <input name="name_activity" type="text" class="form-control" placeholder="Nama Kegiatan" value="{{ isset($data['name']) ? $data['name'] : null }}">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Tingkat</label>
            <select id="level" name="level" class="form-control select2">
                <option value="">-- Pilih --</option>
            </select>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Tanggal Pelaksanaan</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-calendar"></i></span>
                </div>
                <input name="implementation_date" type="date" class="form-control input-date-moment" data-date-format="DD/MMM/YYYY" placeholder="dd/mm/yyyy" value="{{ isset($data['implementDate']) ? $data['implementDate'] : null }}">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Peran</label>
            <select id="role" name="role" class="form-control select2" readonly>
                <option value="">-- Pilih --</option>
            </select>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label>Upload Dokumen</label>
            <div class="input-group">
                <div class="custom-file">
                    <input name="file_document" type="file" class="custom-file-input" id="exampleInputFile">
                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                </div>
            </div>
        </div>
    </div>
</div>

@push('custom-script')
<script>
// Init Custom File Input
bsCustomFileInput.init();

// On Type Activity is changes
$('#typeActivity').on('change', function() {
  // Get value data
  let valUrl = @json(route('ajax.getOptLevel'));
  let valId = $(this).val();

  // Clear option in select
  $('#level').html('<option value="">-- Pilih --</option>');
  $('#role').html('<option value="">-- Pilih --</option>');

  // Check value null in selected value
  if (valId == null) {
    return;
  }

  // Request data to API
  $.ajax({
    url: valUrl,
    data: {
      typeActivity: valId
    },
    method: 'POST'
  })
  .done(function(res) {
    $.each(res, function( index, value ) {
      $('#level').append('<option value="' + index + '">' + value + '</option>');
    });
  });
});

// On Level is changes
$('#level').on('change', function() {
  // Get value data
  let valUrl = @json(route('ajax.getOptRoleAct'));
  let valTypeActId = $('#typeActivity').val();
  let valLevelId = $(this).val();

  // Clear option in select
  $('#role').html('<option value="">-- Pilih --</option>');

  // Check value null in selected value
  if (valTypeActId == null || valLevelId == null) {
    return;
  }

  // Request data to API
  $.ajax({
    url: valUrl,
    data: {
      typeActivity: valTypeActId,
      level: valLevelId
    },
    method: 'POST'
  })
  .done(function(res) {
    $.each(res, function( index, value ) {
      $('#role').append('<option value="' + index + '">' + value + '</option>');
    });
  });
});
</script>
@endpush