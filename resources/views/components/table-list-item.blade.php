@push('custom-style')
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
@endpush

<div class="table-responsive">
    <table class="table table-bordered data-table">
        <thead>
            <tr>
                @foreach ($headerColumns as $row)
                    @php 
                        $name = $row['name'];
                        $style = isset($row['style']) ? $row['style'] : '';
                    @endphp

                    <th {{ $style }}>{{ $name }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>

<form id="formRowSubmit" action="" method="POST"> @csrf @method('DELETE') </form>

@push('custom-script')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
  $(function () {
    let dataColumns = @json($dataColumns);

    let listDataTable = $('.data-table').DataTable({
        responsive: true,
        serverSide: true,
        ajax: "{{ $urlData }}",
        columns: dataColumns
    });

    $(document).on('click', '.btn-row-submit', function() {
        let valTextConfirm = $(this).data('text-confirm');
        let valUrl = $(this).data('url');
        let valMethod = $(this).data('method');

        Swal.fire({
            title: valTextConfirm,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak'
        })
        .then((result) => {
            if (result.value == true) {
                $('#formRowSubmit').attr('action', valUrl);
                $('#formRowSubmit input[name=_method]').val(valMethod);
                $('#formRowSubmit').submit();
            }
        });
    });
  });
</script>
@endpush