<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class RegisterTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->clickLink('Daftar') // Click button register
                ->assertSee('Daftar')
                ->type('no_id', '101616300')
                ->type('name', 'Aditya Raihan')
                ->type('email', 'aditya@dusk.com')
                ->type('password', '123123123')
                ->type('confPassword', '123123123')
                ->type('phone', '')
                ->type('address', '')
                ->select('semester', '1')
                ->click('@submit-button')
                ->assertPathIs('/profile/started')
                ->assertSee('Minat & Bakat');
        });
    }
}