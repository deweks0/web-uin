<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MParentActivitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tableName = 'm_parent_activities';
        $dateNow = date("Y-m-d H:i:s");

        $codePK = 'PENALARAN_KEILMUAN';
        $codeOK = 'ORGANISASI_KEPEMIMPINAN';
        $codeMB = 'MINAT_BAKAT';
        $codePM = 'PENGABDIAN_MASYARAKAT';
        $codeKW = 'KEWIRAUSAHAAN';

        $arrData = [
            [ 'code' => $codePK, 'name' => 'Penalaran dan Keilmuwan', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codeOK, 'name' => 'Organisasi dan Kepemimpinan', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codeMB, 'name' => 'Minat dan Bakat', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codePM, 'name' => 'Pengabdian pada Masyarakat', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'code' => $codeKW, 'name' => 'Kewirausahaan', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
        ];

        foreach ($arrData as $row) {
            DB::table($tableName)->insert($row);
        }
    }
}