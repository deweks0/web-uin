<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeActivitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tableName = 'type_activities';
        $dateNow = date("Y-m-d H:i:s");

        $arrData = [
            [ 'activity_id' => '1', 'level_id' => '1', 'role_activity_id' => '1', 'point' => 2.5, 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'activity_id' => '1', 'level_id' => '1', 'role_activity_id' => '2', 'point' => 1.5, 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'activity_id' => '1', 'level_id' => '1', 'role_activity_id' => '3', 'point' => 1, 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'activity_id' => '1', 'level_id' => '2', 'role_activity_id' => '4', 'point' => 3.5, 'created_at' => $dateNow, 'updated_at' => $dateNow ],
        ];

        foreach ($arrData as $row) {
            DB::table($tableName)->insert($row);
        }
    }
}