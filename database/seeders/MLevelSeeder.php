<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class MLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tableName = 'm_levels';
        $dateNow = date("Y-m-d H:i:s");

        $arrData = [
            [ 'name' => 'Regional', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Provinsi', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Nasional', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Internasional', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Universitas/Fakultas', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Publikasi', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Tidak Publikasi', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Universitas', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Fakultas', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Intra Fakultas', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Intra Universitas', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Umum', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Daerah/Wilayah', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Angkatan', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Kelas', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Regional/Nasional', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Komunitas', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Regional/Provinsi', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Media Online', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Media Cetak', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Langsung (Konser & Pameran)', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Mandiri', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
            [ 'name' => 'Kemitraan', 'created_at' => $dateNow, 'updated_at' => $dateNow ],
        ];

        foreach ($arrData as $row) {
            DB::table($tableName)->insert($row);
        }
    }
}
