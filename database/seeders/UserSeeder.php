<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tableName = 'users';
        $dateNow = date("Y-m-d H:i:s");

        DB::table($tableName)->insert([
            'no_id' => '000000000',
            'name' => 'Super User',
            'email' => 'superuser@app.com', 
            'password' => Hash::make('superuser'),
            'role_id' => '1',
            'phone' => '088888888888',
            'address' => '',
            'created_at' => $dateNow, 
            'updated_at' => $dateNow 
        ]);

        DB::table($tableName)->insert([
            'no_id' => '456789876543238',
            'name' => 'Superman App',
            'email' => 'superman@app.com', 
            'password' => Hash::make('123123123'),
            'role_id' => '1',
            'phone' => '089890812371',
            'address' => '',
            'created_at' => $dateNow, 
            'updated_at' => $dateNow 
        ]);
    }
}
