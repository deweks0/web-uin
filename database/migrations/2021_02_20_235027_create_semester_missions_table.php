<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSemesterMissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('semester_missions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('semester_id');
            $table->string('parent_activity_id')->index();
            $table->double('target_total_score', 8, 2)->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('semester_id')->references('id')->on('m_semesters');
            $table->foreign('parent_activity_id')->references('code')->on('m_parent_activities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('semester_missions');
    }
}
