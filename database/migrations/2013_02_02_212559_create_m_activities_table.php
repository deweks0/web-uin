<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_activities', function (Blueprint $table) {
            $table->id();
            $table->string('code', 30)->index();
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('code')->references('code')->on('m_parent_activities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_activities');
    }
}
